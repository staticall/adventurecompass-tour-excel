<?php
namespace AdventureCompass;

use Symfony\Component\Filesystem\Filesystem;

trait TempPathTrait
{
    protected $_tempPath;

    public function setTempPath($path = null)
    {
        if ($path === null) {
            $path = $this->generateTempPath();
        }

        $this->_tempPath = $path;

        return $this;
    }

    public function getTempPath()
    {
        $fs = new Filesystem;

        $path = $this->_tempPath;

        if (!empty($path) && !$fs->exists($path)) {
            $fs->mkdir($path);
        }

        return $path;
    }

    public function generateTempPath()
    {
        return Common::transformWinToLinuxPath(sys_get_temp_dir()) . '/' . uniqid();
    }
}