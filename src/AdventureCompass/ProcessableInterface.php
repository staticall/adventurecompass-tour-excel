<?php
namespace AdventureCompass;

interface ProcessableInterface
{
    public function process();
}