<?php
namespace AdventureCompass;

use AdventureCompass\ProcessableInterface;

use AdventureCompass\BatchProcess\Zip;
use Symfony\Component\Filesystem\Filesystem;

class BatchProcess implements ProcessableInterface
{
    use TempPathTrait;

    /**
     * @var string|null
     */
    protected $_file;

    public function __construct($file)
    {
        $this->setFile($file);
    }

    public function setFile($file)
    {
        if (!is_readable($file) || !is_file($file)) {
            throw new BatchProcess\Exception("Either file doesn't exists or can not be read '{$file}'");
        }

        $this->_file = $file;

        return $this;
    }

    public function getFile()
    {
        return $this->_file;
    }

    public function process()
    {
        try {
            $zip = new Zip($this->getFile());
        } catch (Zip\Exception $e) {
            throw new BatchProcess\Exception('Error catched while processing, original error - ' . $e->getMessage(), null, $e);
        }

        if ($this->getTempPath()) {
            $zip->setTempPath($this->getTempPath());
        }

        return $zip->process();
    }
}