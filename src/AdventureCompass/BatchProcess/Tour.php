<?php
namespace AdventureCompass\BatchProcess;

use AdventureCompass\ProcessableInterface;

class Tour implements ProcessableInterface
{
    protected $_folder;

    public function __construct($folderName)
    {
        $this->setFolder($folderName);
    }

    public function setFolder($folder)
    {
        $folder = rtrim($folder, '/');

        if (!is_readable($folder) || !is_dir($folder)) {
            throw new Tour\Exception("Either folder doesn't exists or can not be read '{$folder}'");
        }

        $result = $this->getXlsx($folder);

        if (empty($result) || count($result) !== 1) {
            throw new Tour\Exception('Folder has invalid structure, must have exactly one .xlsx file');
        }

        $this->_folder = $folder;

        return $this;
    }

    public function getFolder()
    {
        return $this->_folder;
    }

    public function process()
    {
        $xlsx = $this->getXlsx();

        $excel  = new Tour\Excel(reset($xlsx));
        $photos = new Tour\Photo($this->getPhotos());
        $videos = new Tour\Video($this->getVideos());
        $pdfs   = new Tour\Pdf($this->getPdfs());

        return [
            'tour'   => $excel->process(),
            'photos' => $photos->process(),
            'videos' => $videos->process(),
            'pdfs'   => $pdfs->process(),
        ];
    }

    public function getXlsx($folder = null)
    {
        if ($folder === null) {
            $folder = $this->getFolder();
        }

        $found = glob($folder . '/*.xlsx');

        if (empty($found)) {
            $found = glob('*.xlsx');
        }

        return $found;
    }

    public function getPhotos($folder = null)
    {
        if ($folder === null) {
            $folder = $this->getFolder();
        }

        $found = glob($folder . '/PHOTO/*.*');

        return $found;
    }

    public function getVideos($folder = null)
    {
        if ($folder === null) {
            $folder = $this->getFolder();
        }

        $found = glob($folder . '/VIDEO/*.*');

        return $found;
    }

    public function getPdfs($folder = null)
    {
        if ($folder === null) {
            $folder = $this->getFolder();
        }

        $found = glob($folder . '/*.pdf');

        return $found;
    }
}