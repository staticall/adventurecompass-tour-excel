<?php
namespace AdventureCompass\BatchProcess;

use AdventureCompass\TempPathTrait;
use Alchemy\Zippy;

use Alchemy\Zippy\Exception\RuntimeException;

use AdventureCompass\ProcessableInterface;

use Symfony\Component\Finder\Glob;

class Zip implements ProcessableInterface
{
    use TempPathTrait;

    const DEFAULT_GLOB_PATTERN = '**/*.xlsx';

    /** @var Zippy\Archive\Archive|null */
    protected $_zip;

    public function __construct($file)
    {
        $this->setZip($file);
    }

    public function setZip($file)
    {
        $zippyClass = Zippy\Archive\Archive::class;

        if (is_object($file) && $file instanceof $zippyClass) {
            $this->_zip = $file;

            return $this;
        }

        if (!is_readable($file) || !is_file($file)) {
            throw new Zip\InvalidArgumentException("Either file doesn't exists or can not be read '{$file}'");
        }

        $zippy = Zippy\Zippy::load();

        try {
            $archive = $zippy->open($file);
        } catch (RuntimeException $e) {
            throw new Zip\InvalidArgumentException('Invalid archive passed, original error - ' . $e->getMessage(), null, $e);
        }

        $this->_zip = $archive;

        return $this;
    }

    public function getZip()
    {
        return $this->_zip;
    }

    public function process()
    {
        $files = $this->getFiles(self::DEFAULT_GLOB_PATTERN);

        if (empty($files)) {
            return [];
        }

        $processed = [];

        /** @var \Alchemy\Zippy\Archive\Member $file */
        foreach ($files as $file) {
            $folder = rtrim(dirname($file->getLocation()), '/') . '/';

            $contents = $this->getFolderContents($folder);

            $tmpPath = $this->getTempPath();

            $tourName = trim($folder, '/');

            $processed[$tourName] = $this->processItem($contents, $tourName, $tmpPath);
        }

        return $processed;
    }

    public function processItem(array $contents, $tourName, $tmpPath)
    {
        $tmpPath = rtrim($tmpPath, '/');

        $this->_zip->extractMembers($contents, $tmpPath);

        $tour = new Tour($tmpPath . '/' . $tourName);

        return $tour->process();
    }

    public function getFolderContents($folderName)
    {
        $parents  = [];
        $allFiles = $this->getFiles();

        foreach ($allFiles as $tmpFile) {
            if (mb_strpos($tmpFile->getLocation(), $folderName) !== 0) {
                continue;
            }

            if ($tmpFile->isDir()) {
                continue;
            }

            $parents[] = $tmpFile;
        }

        return $parents;
    }

    public function getFiles($glob = null)
    {
        $files = $this->_zip->getMembers();

        $regex = null;

        if ($glob) {
            $regex = Glob::toRegex($glob, false, false);
        }

        $formatted = [];

        /** @var \Alchemy\Zippy\Archive\Member $file */
        foreach ($files as $file) {
            if ($regex && !preg_match($regex, $file->getLocation())) {
                continue;
            }

            $formatted[] = $file;
        }

        return $formatted;
    }
}