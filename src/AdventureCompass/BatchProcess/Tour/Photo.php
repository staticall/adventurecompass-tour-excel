<?php
namespace AdventureCompass\BatchProcess\Tour;

use Imagine\Gd\Imagine as ImagineAdapter;

use AdventureCompass\ProcessableInterface;

class Photo implements ProcessableInterface
{
    /**
     * @var ImagineAdapter|null
     */
    static protected $_imagine;

    /**
     * @var array
     */
    protected $_photos;

    public function __construct(array $photos)
    {
        $this->setPhotos($photos);
    }

    public function setPhotos(array $photos)
    {
        $validated = [];

        foreach ($photos as $file) {
            try {
                $this->validate($file);

                $validated[$file] = $file;
            } catch (Photo\Exception $e) {
                continue;
            }
        }

        $this->_photos = $validated;

        return $this;
    }

    public function getPhotos()
    {
        return $this->_photos;
    }

    public function process()
    {
        return $this->getPhotos();
    }

    public function validate($file)
    {
        if (!is_readable($file) || !is_file($file)) {
            throw new Photo\InvalidArgumentException('Either file doesn\'t exists or is not readable');
        }

        $imagine = self::getImagine();

        try {
            $imagine->open($file);
        } catch (\Exception $e) {
            throw new Photo\InvalidArgumentException('Invalid image, original error: ' . $e->getMessage(), null, $e);
        }

        return true;
    }

    static public function getImagine()
    {
        if (!self::$_imagine) {
            self::resetImagineInstance();
        }

        return self::$_imagine;
    }

    static public function resetImagineInstance()
    {
        self::$_imagine = new ImagineAdapter;

        return self::$_imagine;
    }
}