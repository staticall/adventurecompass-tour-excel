<?php
namespace AdventureCompass\BatchProcess\Tour;

use AdventureCompass\ProcessableInterface;

class Video implements ProcessableInterface
{
    protected $_videos = [];

    public function __construct(array $videos)
    {
        $this->setVideos($videos);
    }

    public function setVideos(array $videos)
    {
        $validated = [];

        $supportedExtensions = [
            'mp4',
        ];

        foreach ($videos as $video) {
            $ext = pathinfo($video, PATHINFO_EXTENSION);

            if (!in_array($ext, $supportedExtensions, true)) {
                continue;
            }

            $validated[$video] = $video;
        }

        $this->_videos = $validated;

        return $this;
    }

    public function getVideos()
    {
        return $this->_videos;
    }

    public function process()
    {
        return $this->getVideos();
    }
}