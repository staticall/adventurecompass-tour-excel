<?php
namespace AdventureCompass\BatchProcess\Tour;

use AdventureCompass\BatchProcess\Exception as BaseException;

class Exception extends BaseException
{
}