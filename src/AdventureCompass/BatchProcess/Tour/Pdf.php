<?php
namespace AdventureCompass\BatchProcess\Tour;

use AdventureCompass\ProcessableInterface;

class Pdf implements ProcessableInterface
{
    protected $_pdfs = [];

    public function __construct(array $pdfs)
    {
        $this->setPdfs($pdfs);
    }

    public function setPdfs(array $pdfs)
    {
        $validated = [];

        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        foreach ($pdfs as $pdf) {
            if (finfo_file($finfo, $pdf) !== 'application/pdf') {
                continue;
            }

            $validated[$pdf] = $pdf;
        }

        $this->_pdfs = $validated;

        return $this;
    }

    public function getPdfs()
    {
        return $this->_pdfs;
    }
    
    public function process()
    {
        return $this->getPdfs();
    }
}