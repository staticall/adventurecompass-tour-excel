<?php
namespace AdventureCompass\BatchProcess\Tour;

use AdventureCompass\ProcessableInterface;

class Excel implements ProcessableInterface
{
    /**
     * @var \PHPExcel|null
     */
    protected $_xlsx;

    public function __construct($xlsx)
    {
        $this->_xlsx = self::setXlsx($xlsx);
    }

    public function setXlsx($xlsx)
    {
        if (!is_readable($xlsx) || !is_file($xlsx)) {
            throw new Excel\InvalidArgumentException("Either file doesn't exists or can not be read '{$xlsx}'");
        }

        /** @var \PHPExcel_Reader_Excel2007 $reader */
        $reader = \PHPExcel_IOFactory::createReader('Excel2007');

        return $reader->load($xlsx);
    }

    public function getXlsx()
    {
        return $this->_xlsx;
    }

    public function process()
    {
        $sheets = $this->_xlsx->getAllSheets();

        $data = [];

        foreach ($sheets as $sheet) {
            $part = Excel\PartAbstract::factory($sheet->getTitle(), $sheet);

            $reflection = new \ReflectionClass($part);

            $shortClassName = $reflection->getShortName();

            if (!empty($data[$shortClassName])) {
                if (empty($data[$shortClassName][0])) {
                    $data[$shortClassName] = [$data[$shortClassName]];
                }

                $data[$shortClassName][] = $part->getData();
            } else {

                $data[$shortClassName] = $part->getData();
            }
        }

        return $data;
    }
}