<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class Accommodation extends PartAbstract
{
    public function getData()
    {
        return $this->getDataFlatten();
    }

    public function getMap()
    {
        return [
            'A' => 'ACCOMMODATION DESCRIPTION',
        ];
    }

    public function getRequired()
    {
        return [
            'ACCOMMODATION DESCRIPTION',
        ];
    }
}