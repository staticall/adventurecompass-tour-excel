<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class Itinerary extends PartAbstract
{
    public function getData()
    {
        $list = parent::getData();

        usort(
            $list,

            function ($a, $b) {
                return $a['NUM DAY'] - $b['NUM DAY'];
            }
        );

        return $list;
    }

    public function postPrepare(array $data)
    {
        $data['NUM DAY'] = (int)$data['NUM DAY'];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(array $part)
    {
        $isValid = parent::isValid($part);

        if (!$isValid) {
            return false;
        }

        return $part['NUM DAY'] > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getMap()
    {
        return [
            'A' => 'NUM DAY',
            'B' => 'TITLE DAY',
            'C' => 'DESCRIPTION',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequired()
    {
        return [
            'NUM DAY',
            'TITLE DAY',
        ];
    }
}