<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class IncludedNotIncluded extends PartAbstract
{
    public function getData()
    {
        $map = $this->getMap();

        $sheet = $this->getSheet();

        $firstElement = reset($sheet);

        if (in_array($firstElement['A'], array_values($map))) {
            $map = array_shift($sheet);
        }

        $list = [];

        foreach ($map as $m) {
            $list[$m] = [];
        }

        foreach ($sheet as $part) {
            $data = $this->processPart($part, $map);

            if (!$this->isValid($data)) {
                continue;
            }

            $data = $this->postPrepare($data);

            foreach ($data as $name => $value) {
                $list[$name][] = $value;
            }
        }

        return $list;
    }

    public function postPrepare(array $data)
    {
        foreach ($data as $name => $value) {
            if (empty($value)) {
                unset($data[$name]);
            }
        }

        return $data;
    }

    public function isValid(array $data)
    {
        return count(array_filter($data)) > 0;
    }

    public function getMap()
    {
        return [
            'A' => 'INCLUDED',
            'B' => 'NOT INCLUDED',
        ];
    }
}