<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class Custom extends PartAbstract
{
    public function getData()
    {
        return $this->getDataFlatten();
    }

    public function isValid(array $data)
    {
        $filtered = array_filter($data);

        return !empty($filtered);
    }
}