<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class GeneralInfo extends PartAbstract
{
    public function getData()
    {
        $sheet = $this->getSheet();

        $data = [];

        foreach ($sheet as $dataset) {
            $data[$dataset['A']] = $dataset['B'];
        }

        $data = $this->postPrepare($data);

        return $data;
    }

    public function postPrepare(array $data)
    {
        $data = self::processImplodedMinMaxValues($data, 'TOUR PRICE MIN:MAX', 'TOUR PRICE MIN', 'TOUR PRICE MAX');
        $data = self::processImplodedMinMaxValues($data, 'GROUP SIZE MIN:MAX', 'GROUP SIZE MIN', 'GROUP SIZE MAX');

        $data['TOUR PRICE MIN'] = self::processFloat($data['TOUR PRICE MIN']);
        $data['TOUR PRICE MAX'] = self::processFloat($data['TOUR PRICE MAX']);
        $data['GROUP SIZE MIN'] = self::processInteger($data['GROUP SIZE MIN'], true);
        $data['GROUP SIZE MAX'] = self::processInteger($data['GROUP SIZE MAX'], true);

        if (!empty($data['GROUP SIZE MIN']) && empty($data['GROUP SIZE MAX'])) {
            $data['GROUP SIZE MAX'] = $data['GROUP SIZE MIN'];
        }

        if (empty($data['GROUP SIZE MIN']) && !empty($data['GROUP SIZE MAX'])) {
            $data['GROUP SIZE MIN'] = $data['GROUP SIZE MAX'];
        }

        if (array_key_exists('ROUTE COMPLEXITY', $data) && $data['ROUTE COMPLEXITY'] <= 10 && $data['ROUTE COMPLEXITY'] >= 0) {
            $data['ROUTE COMPLEXITY'] = (int)$data['ROUTE COMPLEXITY'];
        } else {
            $data['ROUTE COMPLEXITY'] = null;
        }

        if (array_key_exists('DURATION', $data)) {
            $data['DURATION'] = (int)$data['DURATION'];
        }

        return $data;
    }

    static public function processFloat($value, $precision = 2)
    {
        $value = str_replace(',', '.', $value);

        if (!is_numeric($value)) {
            $value = 0;
        }

        return round($value, $precision);
    }

    static public function processInteger($int, $isPositiveOnly = false)
    {
        if (!is_numeric($int)) {
            $int = 0;
        }

        $int = (int)$int;

        if ($isPositiveOnly && $int < 0) {
            $int = 0;
        }

        return $int;
    }

    static public function processImplodedMinMaxValues(array $data, $key, $minLabel, $maxLabel)
    {
        if (empty($data[$key])) {
            return $data;
        }

        $value = explode(':', $data[$key]);

        foreach ($value as &$v) {
            $v = trim($v);
        }

        unset($v);

        $valueMin = $value[0];
        $valueMax = empty($value[1]) ? $value[0] : $value[1];

        unset($data[$key]);

        $data[$minLabel] = $valueMin;
        $data[$maxLabel] = $valueMax;

        return $data;
    }
}