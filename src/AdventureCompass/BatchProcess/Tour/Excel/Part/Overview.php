<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class Overview extends PartAbstract
{
    public function getData()
    {
        return $this->getDataFlatten();
    }

    public function getMap()
    {
        return [
            'A' => 'OVERVIEW DESCRIPTION',
        ];
    }

    public function getRequired()
    {
        return [
            'OVERVIEW DESCRIPTION',
        ];
    }
}