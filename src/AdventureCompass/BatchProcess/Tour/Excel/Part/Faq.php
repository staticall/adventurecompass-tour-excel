<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class Faq extends PartAbstract
{
    /**
     * {@inheritdoc}
     */
    public function getMap()
    {
        return [
            'A' => 'QUESTION',
            'B' => 'ANSWER',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequired()
    {
        return [
            'QUESTION',
        ];
    }
}