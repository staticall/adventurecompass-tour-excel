<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class Testimonials extends PartAbstract
{
    /**
     * {@inheritdoc}
     */
    public function getMap()
    {
        return [
            'A' => 'AUTHOR NAME',
            'B' => 'REVIEW DESCRIPTION',

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequired()
    {
        return [
            'AUTHOR NAME',
            'REVIEW DESCRIPTION',
        ];
    }
}