<?php
namespace AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

class DatePrice extends PartAbstract
{
    public function postPrepare(array $data)
    {
        if (!empty($data['DATE: DEPARTURE'])) {
            if (is_numeric($data['DATE: DEPARTURE'])) {
                $data['DATE: DEPARTURE'] = date('Y-m-d H:i:s', \PHPExcel_Shared_Date::ExcelToPHP($data['DATE: DEPARTURE']));
            }
        }

        if (!empty($data['DATE: RETURN'])) {
            if (is_numeric($data['DATE: RETURN'])) {
                $data['DATE: RETURN'] = date('Y-m-d H:i:s', \PHPExcel_Shared_Date::ExcelToPHP($data['DATE: RETURN']));
            }
        }

        if (empty($data['DURATION:']) && !empty($data['DATE: DEPARTURE']) && !empty($data['DATE: RETURN'])) {
            $departure = new \DateTime($data['DATE: DEPARTURE']);
            $return    = new \DateTime($data['DATE: RETURN']);

            $data['DURATION:'] = $return->diff($departure)->days;
        }

        $data['DURATION:'] = (int)$data['DURATION:'];

        $data['PRICE (INCL. FLIGHT)'] = (float)str_replace(',', '.', $data['PRICE (INCL. FLIGHT)']);
        $data['PRICE (EXCL. FLIGHT)'] = (float)str_replace(',', '.', $data['PRICE (EXCL. FLIGHT)']);

        return $data;
    }

    public function getMap()
    {
        return [
            'A' => 'DATE: DEPARTURE',
            'B' => 'DATE: RETURN',
            'C' => 'DURATION:',
            'D' => 'PRICE (INCL. FLIGHT)',
            'E' => 'PRICE (EXCL. FLIGHT)',
        ];
    }
}