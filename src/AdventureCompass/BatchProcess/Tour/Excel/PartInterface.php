<?php
namespace AdventureCompass\BatchProcess\Tour\Excel;

interface PartInterface
{
    public function getData();
}