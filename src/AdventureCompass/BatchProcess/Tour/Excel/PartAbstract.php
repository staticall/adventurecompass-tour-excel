<?php
namespace AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom;

use AdventureCompass\Common;

abstract class PartAbstract implements PartInterface
{
    /**
     * @var array
     */
    protected $_sheet = [];

    /**
     * @param \PHPExcel_Worksheet|array $sheet
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($sheet)
    {
        $this->setSheet($sheet);
    }

    /**
     * Creates a new instance of desired part class
     *
     * @param string                    $name
     * @param array|\PHPExcel_Worksheet $sheet
     *
     * @return \Advcompass\Processor\Part\Custom|\Advcompass\Processor\PartAbstract
     */
    static public function factory($name, $sheet)
    {
        $procesorNamespace = __NAMESPACE__ . '\\Part\\';

        $name = str_replace($procesorNamespace, '', $name);

        $expectedPart = $procesorNamespace . Common::classify($name);
        $customPart   = Custom::class;

        if (!class_exists($expectedPart)) {
            return new $customPart($sheet);
        }

        return new $expectedPart($sheet);
    }

    /**
     * Returns current sheet data
     *
     * @return array
     */
    public function getSheet()
    {
        return $this->_sheet;
    }

    /**
     * Changes current sheet data
     *
     * @param \PHPExcel_Worksheet|array $sheet Either instance of PHPExcel sheet or already prepared array
     *
     * @return $this
     *
     * @throws \InvalidArgumentException
     */
    public function setSheet($sheet)
    {
        if (is_object($sheet) && $sheet instanceof \PHPExcel_Worksheet) {
            $sheet = $sheet->toArray(null, true, false, true);
        }

        if (!is_array($sheet)) {
            throw new \InvalidArgumentException('Unknown data type, expected array');
        }

        $this->_sheet = $sheet;

        return $this;
    }

    public function getData()
    {
        $map = $this->getMap();

        $sheet = $this->getSheet();

        $firstElement = reset($sheet);

        if ($firstElement['A'] === reset($map)) {
            $map = array_shift($sheet);
        }

        $list = [];

        foreach ($sheet as $part) {
            $data = $this->processPart($part, $map);

            if (!$this->isValid($data)) {
                continue;
            }

            $data = $this->postPrepare($data);

            $list[] = $data;
        }

        return $list;
    }

    public function processPart(array $part, array $map)
    {
        $data = [];

        foreach ($part as $loc => $value) {
            $name  = $map[$loc];
            $value = trim($value) ?: null;

            $data[$name] = $value;
        }

        return $data;
    }

    public function postPrepare(array $data)
    {
        return $data;
    }

    /**
     * Is compiled part valid or not
     *
     * @param array $part
     *
     * @return bool
     */
    public function isValid(array $part)
    {
        foreach ($this->getRequired() as $req) {
            if (empty($part[$req])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return default map
     *
     * @return array
     */
    public function getMap()
    {
        return [];
    }

    /**
     * Return array of names that MUST be present and not empty in result array
     * Be aware, child classes might include additional validation
     *
     * @return array
     */
    public function getRequired()
    {
        return [];
    }

    public function getDataFlatten()
    {
        $sheet = $this->getSheet();

        $map = $this->getMap();

        if (count($map) === 0 || $sheet[1]['A'] === $map['A']) {
            $map = array_shift($sheet);
        }

        $mapKey = reset($map);

        $data = [
            $mapKey => [],
        ];

        foreach ($sheet as $part) {
            $data[$mapKey][] = current($part);
        }

        $data[$mapKey] = trim(implode("\n", $data[$mapKey]));
        $data[$mapKey] = !empty($data[$mapKey]) ? $data[$mapKey] : null;

        if (count($this->getRequired()) > 0 && !$this->isValid($data)) {
            return [];
        }

        return $data;
    }
}