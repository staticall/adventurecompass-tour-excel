<?php
namespace AdventureCompass;

class Common
{
    /**
     * Convert a word in to the format for a Doctrine class name
     * For example, both 'my_class' and 'MY_CLASS' will be transformed into 'MyClass'
     *
     * @param string $word Word to classify
     *
     * @return string
     */
    static public function classify($word)
    {
        $word = preg_replace('/[^a-zA-Z _-]/', '', $word);

        return str_replace(' ', '', ucwords(strtr(strtolower($word), "_-", '  ')));
    }

    static public function transformWinToLinuxPath($path)
    {
        $path = str_replace('\\', '/', $path);
        $path = preg_replace('|(?<=.)/+|', '/', $path);

        if (substr($path, 1, 1) === ':') {
            $path = ucfirst($path);
        }

        return $path;
    }
}