<?php
namespace Test\AdventureCompass\BatchProcess\SetFile;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess;

class InvalidTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Exception
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'not_exists_for_sure.zip'
     */
    public function testNotExists()
    {
        new BatchProcess('not_exists_for_sure.zip');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Exception
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'not_readable_for_sure.zip'
     */
    public function testNotReadable()
    {
        self::requireFilesystemPermissionSupported();

        new BatchProcess('tests/datasets/zips/not_readable.zip');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Exception
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'tests/'
     */
    public function testNotFile()
    {
        new BatchProcess('tests/');
    }
}