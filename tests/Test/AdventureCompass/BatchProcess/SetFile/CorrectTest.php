<?php
namespace Test\AdventureCompass\BatchProcess\SetFile;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess;

class CorrectTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testDefaults()
    {
        $reflection = new \ReflectionClass(BatchProcess::class);

        $defaults = $reflection->getDefaultProperties();

        self::assertNull($defaults['_file']);
    }

    public function testCorrect()
    {
        $prop = $this->getSecureProperty(BatchProcess::class, '_file');

        $file = 'tests/datasets/zips/empty.zip';

        $batchProcess = new BatchProcess($file);

        self::assertTrue(is_string($prop->getValue($batchProcess)));

        self::assertSame($file, $batchProcess->getFile());
    }
}