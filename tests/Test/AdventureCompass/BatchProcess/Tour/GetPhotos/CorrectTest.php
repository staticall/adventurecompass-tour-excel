<?php
namespace Test\AdventureCompass\BatchProcess\Tour\GetPhotos;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class CorrectTest extends UnitTestCase
{
    public function testCorrectPresetFolder()
    {
        $folderName = 'tests/datasets/folders/photo/!success';

        $tour = new Tour($folderName);

        $photos = $tour->getPhotos();

        $expected = [
            $folderName . '/PHOTO/1.jpeg',
            $folderName . '/PHOTO/2.jpg',
            $folderName . '/PHOTO/3.png',
            $folderName . '/PHOTO/4.gif',
        ];

        self::assertSame($expected, $photos);
    }

    public function testCorrectSetFolder()
    {
        $folderName = 'tests/datasets/folders/photo/!success';

        $tour = new Tour($folderName);

        $tour->setFolder($folderName . '2');

        $photos = $tour->getPhotos();

        $expected = [
            $folderName . '2/PHOTO/21.jpeg',
            $folderName . '2/PHOTO/22.jpg',
            $folderName . '2/PHOTO/23.png',
            $folderName . '2/PHOTO/24.gif',
        ];

        self::assertSame($expected, $photos);
    }

    public function testCorrectCustomFolder()
    {
        $folderName = 'tests/datasets/folders/photo/!success';

        $tour = new Tour($folderName);

        $photos = $tour->getPhotos($folderName . '2');

        $expected = [
            $folderName . '2/PHOTO/21.jpeg',
            $folderName . '2/PHOTO/22.jpg',
            $folderName . '2/PHOTO/23.png',
            $folderName . '2/PHOTO/24.gif',
        ];

        self::assertSame($expected, $photos);
    }
}