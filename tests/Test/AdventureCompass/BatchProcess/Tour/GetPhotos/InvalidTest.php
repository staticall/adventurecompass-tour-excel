<?php
namespace Test\AdventureCompass\BatchProcess\Tour\GetPhotos;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class InvalidTest extends UnitTestCase
{
    public function testEmpty()
    {
        $folderName = 'tests/datasets/folders/photo/!empty';

        $tour = new Tour($folderName);

        $expected = [];

        $photos = $tour->getPhotos();

        self::assertSame($expected, $photos);

        $photos = $tour->getPhotos($folderName);

        self::assertSame($expected, $photos);
    }

    public function testEmptySubfolder()
    {
        $folderName = 'tests/datasets/folders/photo/!emptyphoto';

        $tour = new Tour($folderName);

        $expected = [];

        $photos = $tour->getPhotos();

        self::assertSame($expected, $photos);

        $photos = $tour->getPhotos($folderName);

        self::assertSame($expected, $photos);
    }

    public function testNoFileName()
    {
        $folderName = 'tests/datasets/folders/photo/!noname';

        $tour = new Tour($folderName);

        $expected = [];

        $photos = $tour->getPhotos();

        self::assertSame($expected, $photos);

        $photos = $tour->getPhotos($folderName);

        self::assertSame($expected, $photos);
    }

    public function testNoFileExt()
    {
        $folderName = 'tests/datasets/folders/photo/!noext';

        $tour = new Tour($folderName);

        $expected = [];

        $photos = $tour->getPhotos();

        self::assertSame($expected, $photos);

        $photos = $tour->getPhotos($folderName);

        self::assertSame($expected, $photos);
    }

    public function testNotInSubfolder()
    {
        $folderName = 'tests/datasets/folders/photo/!notinsubfolder';

        $tour = new Tour($folderName);

        $expected = [];

        $photos = $tour->getPhotos();

        self::assertSame($expected, $photos);

        $photos = $tour->getPhotos($folderName);

        self::assertSame($expected, $photos);
    }
}