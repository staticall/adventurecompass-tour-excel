<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Testimonials;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Testimonials as Part;

class NoHeadersTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/testimonials-no-headers.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

        ];

        self::assertSame($expected, $result);
    }
}