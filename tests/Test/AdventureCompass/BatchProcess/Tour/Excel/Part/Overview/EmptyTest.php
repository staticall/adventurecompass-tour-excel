<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Overview;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Overview as Part;

class EmptyTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/overview-empty.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [];

        self::assertSame($expected, $result);
    }
}