<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Itinerary;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Itinerary as Part;

class OnlyOneDayTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/itinerary-only-one-day.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'NUM DAY'     => 1,
                'TITLE DAY'   => 'Depart UK',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}