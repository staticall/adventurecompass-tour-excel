<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Itinerary;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Itinerary as Part;

class MissingDayTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/itinerary-missing-day.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'NUM DAY'     => 1,
                'TITLE DAY'   => 'Depart UK',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 2,
                'TITLE DAY'   => 'Arrive in Russia',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 3,
                'TITLE DAY'   => 'Mineralnye Vody - Emanuil Valley',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 4,
                'TITLE DAY'   => 'Emanuil Valley',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 5,
                'TITLE DAY'   => 'Emanuil Valley - Camp 1',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 6,
                'TITLE DAY'   => 'Acclimatisation ascent to Lenz\'s rocks',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 8,
                'TITLE DAY'   => 'Contingency day',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 9,
                'TITLE DAY'   => 'Summit day',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 10,
                'TITLE DAY'   => 'Depart Russia',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}