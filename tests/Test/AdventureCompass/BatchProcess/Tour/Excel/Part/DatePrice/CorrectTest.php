<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\DatePrice;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\DatePrice as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/date-price.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'DATE: DEPARTURE'      => '2017-01-18 03:00:00',
                'DATE: RETURN'         => '2017-01-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-02-18 03:00:00',
                'DATE: RETURN'         => '2017-02-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-03-18 03:00:00',
                'DATE: RETURN'         => '2017-03-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-04-18 03:00:00',
                'DATE: RETURN'         => '2017-04-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-05-18 03:00:00',
                'DATE: RETURN'         => '2017-05-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-06-18 03:00:00',
                'DATE: RETURN'         => '2017-06-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-07-18 03:00:00',
                'DATE: RETURN'         => '2017-07-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-08-18 03:00:00',
                'DATE: RETURN'         => '2017-08-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
            [
                'DATE: DEPARTURE'      => '2017-09-18 03:00:00',
                'DATE: RETURN'         => '2017-09-20 03:00:00',
                'DURATION:'            => 2,
                'PRICE (INCL. FLIGHT)' => 0.00,
                'PRICE (EXCL. FLIGHT)' => 0.00,
            ],
        ];

        self::assertSame($expected, $result);
    }
}