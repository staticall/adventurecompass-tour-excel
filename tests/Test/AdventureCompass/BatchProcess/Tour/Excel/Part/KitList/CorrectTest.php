<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\KitList;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\KitList as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/kit-list.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'COUNT'       => 1,
                'NAME'        => 'Backpack',
                'DESCRIPTION' => 'Approximately 80L to take your kit to higher camps carrying up to 15kg',
            ],
            [
                'COUNT'       => 2,
                'NAME'        => 'Dry Sacks',
                'DESCRIPTION' => 'Pack some fresh clothing into bags to keep them dry in the event of a total downpour that seeps into your kitbag. Good for quarantining old socks',
            ],
            [
                'COUNT'       => 1,
                'NAME'        => 'Kit Bag',
                'DESCRIPTION' => 'A large duffel bag of 80 - 120L or more to transport your kit. Suitcases and wheeled bags are NOT suitable',
            ],
            [
                'COUNT'       => 1,
                'NAME'        => 'Sleepeng Bag Liner',
                'DESCRIPTION' => 'These liners can be fleece or silk. They can increase the warmth of the sleeping bag and help to keep it clean',
            ],
            [
                'COUNT'       => 1,
                'NAME'        => 'Sleeping Matt',
                'DESCRIPTION' => 'Full length rather than ¾ length ‘self-inflating’ Thermarest or Mammut',
            ],
            [
                'COUNT'       => 1,
                'NAME'        => 'Small Bag',
                'DESCRIPTION' => 'This is for any kit you intend to leave at the hotel and could even simply be a heavy duty plastic bag',
            ],
        ];

        self::assertSame($expected, $result);
    }
}