<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\GeneralInfo;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\GeneralInfo as Part;

class FloatsWithCommaTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/general-info-float-with-commas.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'TOUR NAME'          => 'Kunisaki and Yufuin Walk',
            'ORIGINAL TOUR LINK' => 'http://www.360-expeditions.com/climbing/trips/europe/russia/Elbrus_Seven_summits_climb/itinerary',
            'DESTINATION'        => 'Russia Federation',
            'TYPE OF ADVENTURE'  => 'Walk',
            'GUIDE LANGUAGE'     => 'Russia',
            'ROUTE COMPLEXITY'   => 5,
            'DURATION'           => 14,
            'MAP FILE'           => 'MapFile.pdf',
            'PRESENTATION FILE'  => 'PresentationFile.pdf',
            'TOUR PRICE MIN'     => 1.20,
            'TOUR PRICE MAX'     => 3.10,
            'GROUP SIZE MIN'     => 8,
            'GROUP SIZE MAX'     => 14,
        ];

        self::assertSame($expected, $result);
    }
}