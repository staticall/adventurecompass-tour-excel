<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\GeneralInfo\ProcessImplodedMinMaxValues;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel\Part\GeneralInfo as Part;

class CorrectTest extends UnitTestCase
{
    public function testSingleValue()
    {
        $data = [
            'TEST VALUE' => '1',
        ];

        $expected = [
            'MIN' => '1',
            'MAX' => '1',
        ];

        self::assertSame($expected, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }

    public function testSingleValueAsString()
    {
        $data = [
            'TEST VALUE' => 'Foo',
        ];

        $expected = [
            'MIN' => 'Foo',
            'MAX' => 'Foo',
        ];

        self::assertSame($expected, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }

    public function testSemicolonWithoutSpaces()
    {
        $data = [
            'TEST VALUE' => '1:2',
        ];

        $expected = [
            'MIN' => '1',
            'MAX' => '2',
        ];

        self::assertSame($expected, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }

    public function testSemicolonWithSpaces()
    {
        $data = [
            'TEST VALUE' => '2 : 3',
        ];

        $expected = [
            'MIN' => '2',
            'MAX' => '3',
        ];

        self::assertSame($expected, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }

    public function testSemicolonAndValueWithSpaces()
    {
        $data = [
            'TEST VALUE' => ' 3 : 4 ',
        ];

        $expected = [
            'MIN' => '3',
            'MAX' => '4',
        ];

        self::assertSame($expected, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }
}