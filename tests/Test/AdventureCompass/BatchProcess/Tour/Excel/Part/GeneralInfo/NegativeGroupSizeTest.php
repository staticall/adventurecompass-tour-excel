<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\GeneralInfo;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\GeneralInfo as Part;

class NegativeGroupSizeTest extends UnitTestCase
{
    public function testParsedThroughXlsxSingular()
    {
        $excel = new Excel('tests/datasets/passport/general-info-negative-group-size.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'TOUR NAME'          => 'Kunisaki and Yufuin Walk',
            'ORIGINAL TOUR LINK' => 'http://www.360-expeditions.com/climbing/trips/europe/russia/Elbrus_Seven_summits_climb/itinerary',
            'DESTINATION'        => 'Russia Federation',
            'TYPE OF ADVENTURE'  => 'Walk',
            'GUIDE LANGUAGE'     => 'Russia',
            'ROUTE COMPLEXITY'   => 5,
            'DURATION'           => 14,
            'MAP FILE'           => 'MapFile.pdf',
            'PRESENTATION FILE'  => 'PresentationFile.pdf',
            'TOUR PRICE MIN'     => 0.00,
            'TOUR PRICE MAX'     => 0.00,
            'GROUP SIZE MIN'     => 0,
            'GROUP SIZE MAX'     => 0,
        ];

        self::assertSame($expected, $result);
    }

    public function testParsedThroughXlsxMultiple()
    {
        $excel = new Excel('tests/datasets/passport/general-info-negative-group-size-multiple.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'TOUR NAME'          => 'Kunisaki and Yufuin Walk',
            'ORIGINAL TOUR LINK' => 'http://www.360-expeditions.com/climbing/trips/europe/russia/Elbrus_Seven_summits_climb/itinerary',
            'DESTINATION'        => 'Russia Federation',
            'TYPE OF ADVENTURE'  => 'Walk',
            'GUIDE LANGUAGE'     => 'Russia',
            'ROUTE COMPLEXITY'   => 5,
            'DURATION'           => 14,
            'MAP FILE'           => 'MapFile.pdf',
            'PRESENTATION FILE'  => 'PresentationFile.pdf',
            'TOUR PRICE MIN'     => 0.00,
            'TOUR PRICE MAX'     => 0.00,
            'GROUP SIZE MIN'     => 0,
            'GROUP SIZE MAX'     => 0,
        ];

        self::assertSame($expected, $result);
    }

    public function testParsedThroughXlsxMultipleOnePositive()
    {
        $excel = new Excel('tests/datasets/passport/general-info-negative-group-size-one-positive.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'TOUR NAME'          => 'Kunisaki and Yufuin Walk',
            'ORIGINAL TOUR LINK' => 'http://www.360-expeditions.com/climbing/trips/europe/russia/Elbrus_Seven_summits_climb/itinerary',
            'DESTINATION'        => 'Russia Federation',
            'TYPE OF ADVENTURE'  => 'Walk',
            'GUIDE LANGUAGE'     => 'Russia',
            'ROUTE COMPLEXITY'   => 5,
            'DURATION'           => 14,
            'MAP FILE'           => 'MapFile.pdf',
            'PRESENTATION FILE'  => 'PresentationFile.pdf',
            'TOUR PRICE MIN'     => 0.00,
            'TOUR PRICE MAX'     => 0.00,
            'GROUP SIZE MIN'     => 8,
            'GROUP SIZE MAX'     => 8,
        ];

        self::assertSame($expected, $result);
    }

    public function testParsedThroughXlsxMultipleOtherPositive()
    {
        $excel = new Excel('tests/datasets/passport/general-info-negative-group-size-other-positive.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'TOUR NAME'          => 'Kunisaki and Yufuin Walk',
            'ORIGINAL TOUR LINK' => 'http://www.360-expeditions.com/climbing/trips/europe/russia/Elbrus_Seven_summits_climb/itinerary',
            'DESTINATION'        => 'Russia Federation',
            'TYPE OF ADVENTURE'  => 'Walk',
            'GUIDE LANGUAGE'     => 'Russia',
            'ROUTE COMPLEXITY'   => 5,
            'DURATION'           => 14,
            'MAP FILE'           => 'MapFile.pdf',
            'PRESENTATION FILE'  => 'PresentationFile.pdf',
            'TOUR PRICE MIN'     => 0.00,
            'TOUR PRICE MAX'     => 0.00,
            'GROUP SIZE MIN'     => 14,
            'GROUP SIZE MAX'     => 14,
        ];

        self::assertSame($expected, $result);
    }
}