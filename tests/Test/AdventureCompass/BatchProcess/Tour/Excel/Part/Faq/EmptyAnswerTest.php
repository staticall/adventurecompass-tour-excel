<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Faq;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Faq as Part;

class EmptyAnswerTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/faq-empty-answer.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'QUESTION' => 'Can I bring my skis?',
                'ANSWER'   => null,
            ],

            [
                'QUESTION' => 'How many climbers will be on this expedition?',
                'ANSWER'   => 'Typically 9 to 12 members will be on the trip with you.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}