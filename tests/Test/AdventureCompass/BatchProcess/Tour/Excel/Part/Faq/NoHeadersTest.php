<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Faq;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Faq as Part;

class NoHeadersTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/faq-no-headers.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'QUESTION' => 'Can I bring my skis?',
                'ANSWER'   => 'While it is possible to ski from the summit, we do not offer a ski trip on Elbrus except as a private trip. Contact our office for details.',
            ],

            [
                'QUESTION' => 'How many climbers will be on this expedition?',
                'ANSWER'   => 'Typically 9 to 12 members will be on the trip with you.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}