<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Accommodation;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Accommodation as Part;

class EmptyTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/accommodation-empty.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [];

        self::assertSame($expected, $result);
    }
}