<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Accommodation;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Accommodation as Part;

class NoHeadersTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/accommodation-no-headers.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'ACCOMMODATION DESCRIPTION' => 'Climb Elbrus the highest mountain in Europe. We reach the summit of Mount Elbrus via the pristine North-South traverse adhering to the climbing principles of tackling the mountain on its own terms. The satisfaction that comes from climbing with a small team of equally focused and dedicated mountaineers is extremely rewarding.

The summit is always a bonus but having gained it through your own “blood, sweat and tears” is much more satisfying and memorable than taking the chairlift to the top! Mount Elbrus is a huge double-domed dormant volcano located in the spectacular Caucasus Mountains of Russia. Starting on the Northern side of the mountain we begin a gradual ascent of the mountain, carrying our personal gear and a share of the teams’ camping equipment.

Taking our time to acclimatise we eventually reach our high camp where we refresh our glacier training, we trek to the summit col before heading up to the Western Summit where an amazing panorama awaits. Finally taking advantage of the aging ski infrastructure on the southern side we have a much faster descent to the distinctly Russian flavoured alpine village where we can relax after the climb. Elbru',
        ];

        self::assertSame($expected, $result);
    }
}