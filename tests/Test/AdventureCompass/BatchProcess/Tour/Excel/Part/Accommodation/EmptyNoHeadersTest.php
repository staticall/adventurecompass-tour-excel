<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Accommodation;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Accommodation as Part;

class EmptyNoHeadersTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/accommodation-empty-no-headers.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [];

        self::assertSame($expected, $result);
    }
}