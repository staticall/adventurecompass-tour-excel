<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\IncludedNotIncluded;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\IncludedNotIncluded as Part;

class NotIncludedOnlyTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/included-not-only.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'NOT INCLUDED' => [
                'Personal equipment and excess baggage for domestic flights',
                'Staff/guide gratuities',
                'Items of a personal nature: phone calls, laundry, room service, etc.',
                'Alcoholic beverages',
                'Additional single accommodation (post exped).',
                'Lunch and dinner when city based except where indicated',
            ],
        ];

        self::assertSame($expected, $result);
    }
}