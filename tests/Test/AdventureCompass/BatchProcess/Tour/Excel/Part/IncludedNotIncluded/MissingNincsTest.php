<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\IncludedNotIncluded;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\IncludedNotIncluded as Part;

class MissingNincsTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/included-or-not-missing-nincs.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'INCLUDED' => [
                'International flights',
                'Scheduled hotel nights, based on double or triple occupancy',
                'Food while on the mountain',
                'Scheduled group restaurant meals',
                'Group climbing and cooking gear',
                'Airport transfer to hotel',
            ],

            'NOT INCLUDED' => [
                'Staff/guide gratuities',
                'Additional single accommodation (post exped).',
                'Lunch and dinner when city based except where indicated',
            ],
        ];

        self::assertSame($expected, $result);
    }
}