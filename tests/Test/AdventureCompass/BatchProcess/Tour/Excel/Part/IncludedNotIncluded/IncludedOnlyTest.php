<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\IncludedNotIncluded;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\IncludedNotIncluded as Part;

class IncludedOnlyTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/included-only.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'INCLUDED' => [
                'International flights',
                'Scheduled hotel nights, based on double or triple occupancy',
                'Food while on the mountain',
                'Scheduled group restaurant meals',
                'Airport transfer to hotel',
            ],
        ];

        self::assertSame($expected, $result);
    }
}