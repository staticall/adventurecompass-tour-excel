<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Custom\IsValid;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom as Part;

class EmptyTest extends UnitTestCase
{
    public function testEmptyArray()
    {
        $part = new Part([]);

        self::assertFalse($part->isValid([]));
    }

    public function testMultidimensionalArray()
    {
        $part = new Part([]);

        self::assertFalse($part->isValid([[]]));
    }

    public function testArrayWithNull()
    {
        $part = new Part([]);

        self::assertFalse($part->isValid(['data' => null]));
    }

    public function testArrayWithEmptyString()
    {
        $part = new Part([]);

        self::assertFalse($part->isValid(['data' => '']));
    }

    public function testArrayWithEmptyArray()
    {
        $part = new Part([]);

        self::assertFalse($part->isValid(['data' => []]));
    }

    public function testArrayWithFalse()
    {
        $part = new Part([]);

        self::assertFalse($part->isValid(['data' => false]));
    }
}