<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Custom;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/custom.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'Enter the title of the section' => 'Enter the content of the section',
        ];

        self::assertSame($expected, $result);
    }
}