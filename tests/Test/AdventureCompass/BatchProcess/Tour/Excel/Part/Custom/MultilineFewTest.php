<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Custom;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom as Part;

class MultilineFewTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/custom-multiline-few.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'Enter the title of the section' => 'Enter the content of the section
Enter the content of the section line #2
Enter the content of the section line #3',
        ];

        self::assertSame($expected, $result);
    }
}