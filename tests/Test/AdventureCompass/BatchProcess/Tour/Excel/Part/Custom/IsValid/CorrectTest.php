<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Custom\IsValid;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom as Part;

class CorrectTest extends UnitTestCase
{
    public function testArrayWithNonTrimmedString()
    {
        $part = new Part([]);

        self::assertTrue($part->isValid(['data' => ' ']));
    }

    public function testArrayWithNonZeroInt()
    {
        $part = new Part([]);

        self::assertTrue($part->isValid(['data' => 1]));
    }

    public function testArrayWithString()
    {
        $part = new Part([]);

        self::assertTrue($part->isValid(['data' => 'test']));
    }

    public function testArrayWithTrue()
    {
        $part = new Part([]);

        self::assertTrue($part->isValid(['data' => true]));
    }
}