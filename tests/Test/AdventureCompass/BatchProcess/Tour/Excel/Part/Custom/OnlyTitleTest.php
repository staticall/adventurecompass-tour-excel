<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\Part\Custom;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom as Part;

class OnlyTitleTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $excel = new Excel('tests/datasets/passport/custom-only-title.xlsx');

        $part = new Part($excel->getXlsx()->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'Enter the content of the section' => null,
        ];

        self::assertSame($expected, $result);
    }
}