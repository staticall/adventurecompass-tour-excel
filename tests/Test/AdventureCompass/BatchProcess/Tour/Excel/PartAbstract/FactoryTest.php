<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\PartAbstract\Factory;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;
use AdventureCompass\BatchProcess\Tour\Excel\Part;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom;

class FactoryTest extends UnitTestCase
{
    public function testShouldReturnCustomPart()
    {
        $dataset = [
            '',
            'Custom',
            'Custom1',
            'Custom-1',
            'Custom 1',
            'CUSTOM1',
            'CUSTOM-1',
            'CUSTOM 1',
            'custom1',
            'custom-1',
            'custom 1',
            'UnexistingType',
            'IncludedNotInclud',
            Custom::class,
        ];

        foreach ($dataset as $input) {
            self::assertInstanceOf(Custom::class, PartAbstract::factory($input, []));
        }
    }

    public function testShouldReturnCorrectPart()
    {
        $dataset = [
            Part\Accommodation::class       => Part\Accommodation::class,
            'Accommodation'                 => Part\Accommodation::class,
            Part\DatePrice::class           => Part\DatePrice::class,
            'DatePrice'                     => Part\DatePrice::class,
            Part\Faq::class                 => Part\Faq::class,
            'Faq'                           => Part\Faq::class,
            Part\GeneralInfo::class         => Part\GeneralInfo::class,
            'GeneralInfo'                   => Part\GeneralInfo::class,
            Part\IncludedNotIncluded::class => Part\IncludedNotIncluded::class,
            'IncludedNotIncluded'           => Part\IncludedNotIncluded::class,
            Part\Itinerary::class           => Part\Itinerary::class,
            'Itinerary'                     => Part\Itinerary::class,
            Part\KitList::class             => Part\KitList::class,
            'KitList'                       => Part\KitList::class,
            Part\Overview::class            => Part\Overview::class,
            'Overview'                      => Part\Overview::class,
            Part\Testimonials::class        => Part\Testimonials::class,
            'Testimonials'                  => Part\Testimonials::class,
        ];

        foreach ($dataset as $input => $expected) {
            self::assertInstanceOf($expected, PartAbstract::factory($input, []));
        }
    }
}