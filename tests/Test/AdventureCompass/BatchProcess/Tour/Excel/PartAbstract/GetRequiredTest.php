<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\PartAbstract;

use Mock\AdventureCompass\BatchProcess\Tour\Excel\Part\MockFromAbstract;

use Test\UnitTestCase;

class GetRequiredTest extends UnitTestCase
{
    public function testShouldBeEmpty()
    {
        $part = new MockFromAbstract([]);

        self::assertSame([], $part->getRequired());
    }
}