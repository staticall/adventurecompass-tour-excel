<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\PartAbstract\SetSheet;

use AdventureCompass\BatchProcess\Tour\Excel\Part\Custom as Part;

use Test\UnitTestCase;

class ThrowsExceptionTest extends UnitTestCase
{
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsStringDuringConstruct()
    {
        new Part('test');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsStringDuringCall()
    {
        $part = new Part([]);

        $part->setSheet('test');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsNullDuringConstruct()
    {
        new Part(null);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsNullDuringCall()
    {
        $part = new Part([]);

        $part->setSheet(null);
    }
}