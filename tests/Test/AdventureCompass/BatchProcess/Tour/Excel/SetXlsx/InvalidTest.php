<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\SetXlsx;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

class InvalidTest extends UnitTestCase
{
    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Excel\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'not_exists_for_sure.xlsx'
     */
    public function testNotExists()
    {
        new Excel('not_exists_for_sure.xlsx');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Excel\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'not_readable_for_sure.xlsx'
     */
    public function testNotReadable()
    {
        self::requireFilesystemPermissionSupported();

        new Excel('tests/datasets/passport/not_readable.xlsx');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Excel\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'tests/'
     */
    public function testNotFile()
    {
        new Excel('tests/');
    }
}