<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\SetXlsx;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

class CorrectTest extends UnitTestCase
{
    public function testCorrectFile()
    {
        $excel = new Excel('tests/datasets/passport/full.xlsx');

        $result = $excel->getXlsx();

        self::assertTrue(is_object($result));

        self::assertInstanceOf(\PHPExcel::class, $result);
    }
}