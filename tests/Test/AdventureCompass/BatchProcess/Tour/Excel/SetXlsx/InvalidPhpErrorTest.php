<?php
namespace Test\AdventureCompass\BatchProcess\Tour\Excel\SetXlsx;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Excel;

class InvalidPhpErrorTest extends UnitTestCase
{
    /**
     * @expectedException \ErrorException
     * @expectedExceptionMessage ZipArchive::getFromName(): Invalid or uninitialized Zip object
     * @expectedExceptionCode 2
     */
    public function testNotXlsx()
    {
        set_error_handler(
            function ($errno, $errstr) {
                throw new \ErrorException($errstr, $errno);
            },

            E_WARNING
        );

        new Excel('tests/Mock/AdventureCompass/BatchProcess/Tour/Excel/Part/MockFromAbstract.php');
    }

    public function tearDown()
    {
        restore_error_handler();
    }
}