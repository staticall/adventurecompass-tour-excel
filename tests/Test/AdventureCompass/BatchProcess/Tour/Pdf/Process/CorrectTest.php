<?php
namespace AdventureCompass\BatchProcess\Tour\Pdf\Process;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Pdf;

class CorrectTest extends UnitTestCase
{
    public function testEmptyShouldBeEmpty()
    {
        $pdf = new Pdf(
            [
            ]
        );

        $expected = [
        ];

        self::assertSame($expected, $pdf->getPdfs());
        self::assertSame($expected, $pdf->process());
        self::assertSame($pdf->process(), $pdf->getPdfs());
    }

    public function testShouldFilterOutSimilars()
    {
        $pdf = new Pdf(
            [
                'tests/datasets/folders/pdf/!success/MapFile.pdf',
                'tests/datasets/folders/pdf/!success/MapFile.pdf',
            ]
        );

        $expected = [
            'tests/datasets/folders/pdf/!success/MapFile.pdf' => 'tests/datasets/folders/pdf/!success/MapFile.pdf',
        ];

        self::assertSame($expected, $pdf->getPdfs());
        self::assertSame($expected, $pdf->process());
        self::assertSame($pdf->process(), $pdf->getPdfs());
    }

    public function testShouldFilterOutInvalids()
    {
        $pdf = new Pdf(
            [
                'tests/datasets/folders/pdf/!success/MapFile.pdf',
                'tests/datasets/folders/pdf/!success/PasportTour.xlsx',
            ]
        );

        $expected = [
            'tests/datasets/folders/pdf/!success/MapFile.pdf' => 'tests/datasets/folders/pdf/!success/MapFile.pdf',
        ];

        self::assertSame($expected, $pdf->getPdfs());
        self::assertSame($expected, $pdf->process());
        self::assertSame($pdf->process(), $pdf->getPdfs());
    }

    public function testShouldKeepSupported()
    {
        $pdf = new Pdf(
            [
                'tests/datasets/folders/pdf/!success3/MapFile.pdf',
                'tests/datasets/folders/pdf/!success3/PresentationFile.pdf',
            ]
        );

        $expected = [
            'tests/datasets/folders/pdf/!success3/MapFile.pdf'          => 'tests/datasets/folders/pdf/!success3/MapFile.pdf',
            'tests/datasets/folders/pdf/!success3/PresentationFile.pdf' => 'tests/datasets/folders/pdf/!success3/PresentationFile.pdf',
        ];

        self::assertSame($expected, $pdf->getPdfs());
        self::assertSame($expected, $pdf->process());
        self::assertSame($pdf->process(), $pdf->getPdfs());
    }
}