<?php
namespace Test\AdventureCompass\BatchProcess\Tour\SetFolder;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class CorrectTest extends UnitTestCase
{
    public function testCorrectFolder()
    {
        $folderName = 'tests/datasets/folders/tour/!success';

        $tour = new Tour($folderName);

        self::assertSame($folderName, $tour->getFolder());
    }
}