<?php
namespace Test\AdventureCompass\BatchProcess\Tour\SetFolder;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class InvalidTest extends UnitTestCase
{
    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Exception
     * @expectedExceptionMessage Either folder doesn't exists or can not be read 'tests/datasets/folders/tour/!notexists'
     */
    public function testNotExists()
    {
        $folderName = 'tests/datasets/folders/tour/!notexists';

        new Tour($folderName);
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Exception
     * @expectedExceptionMessage Either folder doesn't exists or can not be read 'tests/datasets/folders/tour/!notreadable'
     */
    public function testNotReadable()
    {
        self::requireFilesystemPermissionSupported();

        $folderName = 'tests/datasets/folders/tour/!notreadable';

        new Tour($folderName);
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Exception
     * @expectedExceptionMessage Either folder doesn't exists or can not be read 'tests/datasets/folders/tour/!success/PasportTour.xlsx'
     */
    public function testNotFolder()
    {
        $folderName = 'tests/datasets/folders/tour/!success/PasportTour.xlsx';

        new Tour($folderName);
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Exception
     * @expectedExceptionMessage Folder has invalid structure, must have exactly one .xlsx file
     */
    public function testNoXlsxInside()
    {
        $folderName = 'tests/datasets/folders/tour/!noxlsx';

        new Tour($folderName);
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Tour\Exception
     * @expectedExceptionMessage Folder has invalid structure, must have exactly one .xlsx file
     */
    public function testMoreThanOneXlsxInside()
    {
        $folderName = 'tests/datasets/folders/tour/!morethanone';

        new Tour($folderName);
    }
}