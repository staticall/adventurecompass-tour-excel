<?php
namespace Test\AdventureCompass\BatchProcess\Tour\GetXlsx;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class CorrectTest extends UnitTestCase
{
    public function testCorrectPresetFolder()
    {
        $folderName = 'tests/datasets/folders/tour/!success';

        $tour = new Tour($folderName);

        $xlsx = $tour->getXlsx();

        self::assertSame([$folderName . '/PasportTour.xlsx'], $xlsx);
    }

    public function testCorrectSetFolder()
    {
        $folderName = 'tests/datasets/folders/tour/!success';

        $tour = new Tour($folderName);

        $tour->setFolder($folderName . '2');

        $xlsx = $tour->getXlsx();

        self::assertSame([$folderName . '2/PasportTour2.xlsx'], $xlsx);
    }

    public function testCorrectCustomFolder()
    {
        $folderName = 'tests/datasets/folders/tour/!success';

        $tour = new Tour($folderName);

        $xlsx = $tour->getXlsx($folderName . '2');

        self::assertSame([$folderName . '2/PasportTour2.xlsx'], $xlsx);
    }
}