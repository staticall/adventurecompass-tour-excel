<?php
namespace AdventureCompass\BatchProcess\Tour\Video\SetVideos;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Video;

class CorrectTest extends UnitTestCase
{
    public function testEmptyShouldBeEmpty()
    {
        $video = new Video(
            [
            ]
        );

        $expected = [
        ];

        self::assertSame($expected, $video->getVideos());
    }

    public function testShouldFilterOutSimilars()
    {
        $video = new Video(
            [
                'tests/datasets/folders/video/!success/VIDEO/simple.mp4',
                'tests/datasets/folders/video/!success/VIDEO/simple.mp4',
            ]
        );

        $expected = [
            'tests/datasets/folders/video/!success/VIDEO/simple.mp4' => 'tests/datasets/folders/video/!success/VIDEO/simple.mp4',
        ];

        self::assertSame($expected, $video->getVideos());
    }

    public function testShouldFilterOutInvalids()
    {
        $video = new Video(
            [
                'tests/datasets/folders/video/!success/VIDEO/simple.mp4',
                'tests/datasets/folders/video/!success/PasportTour.xlsx',
            ]
        );

        $expected = [
            'tests/datasets/folders/video/!success/VIDEO/simple.mp4' => 'tests/datasets/folders/video/!success/VIDEO/simple.mp4',
        ];

        self::assertSame($expected, $video->getVideos());
    }

    public function testShouldKeepSupported()
    {
        $video = new Video(
            [
                'tests/datasets/folders/video/!success/VIDEO/simple.mp4',
                'tests/datasets/folders/video/!success/VIDEO/simple2.mp4',
            ]
        );

        $expected = [
            'tests/datasets/folders/video/!success/VIDEO/simple.mp4'  => 'tests/datasets/folders/video/!success/VIDEO/simple.mp4',
            'tests/datasets/folders/video/!success/VIDEO/simple2.mp4' => 'tests/datasets/folders/video/!success/VIDEO/simple2.mp4',
        ];

        self::assertSame($expected, $video->getVideos());
    }
}