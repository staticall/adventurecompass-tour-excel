<?php
namespace Test\AdventureCompass\BatchProcess\Tour\GetVideos;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class CorrectTest extends UnitTestCase
{
    public function testCorrectPresetFolder()
    {
        $folderName = 'tests/datasets/folders/video/!success';

        $tour = new Tour($folderName);

        $videos = $tour->getVideos();

        $expected = [
            $folderName . '/VIDEO/small.mp4',
            $folderName . '/VIDEO/small2.mp4',
        ];

        self::assertSame($expected, $videos);
    }

    public function testCorrectSetFolder()
    {
        $folderName = 'tests/datasets/folders/video/!success';

        $tour = new Tour($folderName);

        $tour->setFolder($folderName . '2');

        $videos = $tour->getVideos();

        $expected = [
            $folderName . '2/VIDEO/small.mp4',
        ];

        self::assertSame($expected, $videos);
    }

    public function testCorrectCustomFolder()
    {
        $folderName = 'tests/datasets/folders/video/!success';

        $tour = new Tour($folderName);

        $videos = $tour->getVideos($folderName . '2');

        $expected = [
            $folderName . '2/VIDEO/small.mp4',
        ];

        self::assertSame($expected, $videos);
    }
}