<?php
namespace Test\AdventureCompass\BatchProcess\Tour\GetVideos;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class InvalidTest extends UnitTestCase
{
    public function testEmpty()
    {
        $folderName = 'tests/datasets/folders/video/!empty';

        $tour = new Tour($folderName);

        $expected = [];

        $videos = $tour->getVideos();

        self::assertSame($expected, $videos);

        $videos = $tour->getVideos($folderName);

        self::assertSame($expected, $videos);
    }

    public function testEmptySubfolder()
    {
        $folderName = 'tests/datasets/folders/video/!emptyvideo';

        $tour = new Tour($folderName);

        $expected = [];

        $videos = $tour->getVideos();

        self::assertSame($expected, $videos);

        $videos = $tour->getVideos($folderName);

        self::assertSame($expected, $videos);
    }

    public function testNoFileName()
    {
        $folderName = 'tests/datasets/folders/video/!noname';

        $tour = new Tour($folderName);

        $expected = [];

        $videos = $tour->getVideos();

        self::assertSame($expected, $videos);

        $videos = $tour->getVideos($folderName);

        self::assertSame($expected, $videos);
    }

    public function testNoFileExt()
    {
        $folderName = 'tests/datasets/folders/video/!noext';

        $tour = new Tour($folderName);

        $expected = [];

        $videos = $tour->getVideos();

        self::assertSame($expected, $videos);

        $videos = $tour->getVideos($folderName);

        self::assertSame($expected, $videos);
    }

    public function testNotInSubfolder()
    {
        $folderName = 'tests/datasets/folders/video/!notinsubfolder';

        $tour = new Tour($folderName);

        $expected = [];

        $videos = $tour->getVideos();

        self::assertSame($expected, $videos);

        $videos = $tour->getVideos($folderName);

        self::assertSame($expected, $videos);
    }
}