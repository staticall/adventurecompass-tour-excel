<?php
namespace AdventureCompass\BatchProcess\Tour\Photo\Validate;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Photo;

class NotImageTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        $prop = $this->getSecureProperty(Photo::class, '_imagine');

        $prop->setValue(null, null);
    }

    /**
     * @throws \AdventureCompass\BatchProcess\Tour\Photo\InvalidArgumentException
     *
     * @expectedException \AdventureCompass\BatchProcess\Tour\Photo\InvalidArgumentException
     * @expectedExceptionMessage Invalid image, original error: Unable to open image tests/datasets/test.txt
     */
    public function testTextFile()
    {
        $photo = new Photo([]);

        $photo->validate('tests/datasets/test.txt');
    }
}