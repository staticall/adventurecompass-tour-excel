<?php
namespace AdventureCompass\BatchProcess\Tour\Photo\Validate;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Photo;

class CorrectTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        $prop = $this->getSecureProperty(Photo::class, '_imagine');

        $prop->setValue(null, null);
    }

    public function testJpeg()
    {
        $photo = new Photo([]);

        self::assertTrue($photo->validate('tests/datasets/photo/corrects/1.jpeg'));
    }
}