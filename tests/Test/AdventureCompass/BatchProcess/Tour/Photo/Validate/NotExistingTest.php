<?php
namespace AdventureCompass\BatchProcess\Tour\Photo\Validate;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Photo;

class NotExistingTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        $prop = $this->getSecureProperty(Photo::class, '_imagine');

        $prop->setValue(null, null);
    }

    /**
     * @throws \AdventureCompass\BatchProcess\Tour\Photo\InvalidArgumentException
     *
     * @expectedException \AdventureCompass\BatchProcess\Tour\Photo\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or is not readable
     */
    public function testNotExisting()
    {
        $photo = new Photo([]);

        $photo->validate('definitely_not_existing.file');
    }

    /**
     * @throws \AdventureCompass\BatchProcess\Tour\Photo\InvalidArgumentException
     *
     * @expectedException \AdventureCompass\BatchProcess\Tour\Photo\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or is not readable
     */
    public function testExistsButFolder()
    {
        $photo = new Photo([]);

        $photo->validate('tests/');
    }
}