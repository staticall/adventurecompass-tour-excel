<?php
namespace AdventureCompass\BatchProcess\Tour\Photo\Process;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Photo;

class CorrectTest extends UnitTestCase
{
    public function testEmptyShouldBeEmpty()
    {
        $photo = new Photo(
            [
            ]
        );

        $expected = [
        ];

        self::assertSame($expected, $photo->getPhotos());
        self::assertSame($expected, $photo->process());
        self::assertSame($photo->process(), $photo->getPhotos());
    }

    public function testShouldFilterOutSimilars()
    {
        $photo = new Photo(
            [
                'tests/datasets/folders/photo/!success/PHOTO/1.jpeg',
                'tests/datasets/folders/photo/!success/PHOTO/1.jpeg',
            ]
        );

        $expected = [
            'tests/datasets/folders/photo/!success/PHOTO/1.jpeg' => 'tests/datasets/folders/photo/!success/PHOTO/1.jpeg',
        ];

        self::assertSame($expected, $photo->getPhotos());
        self::assertSame($expected, $photo->process());
        self::assertSame($photo->process(), $photo->getPhotos());
    }

    public function testShouldFilterOutInvalids()
    {
        $photo = new Photo(
            [
                'tests/datasets/folders/photo/!success/PHOTO/1.jpeg',
                'tests/datasets/folders/photo/!success/PasportTour.xlsx',
            ]
        );

        $expected = [
            'tests/datasets/folders/photo/!success/PHOTO/1.jpeg' => 'tests/datasets/folders/photo/!success/PHOTO/1.jpeg',
        ];

        self::assertSame($expected, $photo->getPhotos());
        self::assertSame($expected, $photo->process());
        self::assertSame($photo->process(), $photo->getPhotos());
    }

    public function testShouldKeepSupported()
    {
        $photo = new Photo(
            [
                'tests/datasets/folders/photo/!success/PHOTO/1.jpeg',
                'tests/datasets/folders/photo/!success/PHOTO/2.jpg',
                'tests/datasets/folders/photo/!success/PHOTO/3.png',
            ]
        );

        $expected = [
            'tests/datasets/folders/photo/!success/PHOTO/1.jpeg' => 'tests/datasets/folders/photo/!success/PHOTO/1.jpeg',
            'tests/datasets/folders/photo/!success/PHOTO/2.jpg'  => 'tests/datasets/folders/photo/!success/PHOTO/2.jpg',
            'tests/datasets/folders/photo/!success/PHOTO/3.png'  => 'tests/datasets/folders/photo/!success/PHOTO/3.png',
        ];

        self::assertSame($expected, $photo->getPhotos());
        self::assertSame($expected, $photo->process());
        self::assertSame($photo->process(), $photo->getPhotos());
    }
}