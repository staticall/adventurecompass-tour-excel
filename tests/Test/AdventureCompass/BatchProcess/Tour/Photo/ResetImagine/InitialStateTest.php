<?php
namespace AdventureCompass\BatchProcess\Tour\Photo\ResetImagine;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour\Photo;

class InitialStateTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        $prop = $this->getSecureProperty(Photo::class, '_imagine');

        $prop->setValue(null, null);
    }

    public function testReflected()
    {
        $reflection = new \ReflectionClass(Photo::class);

        $properties = $reflection->getDefaultProperties();

        self::assertArrayHasKey('_imagine', $properties);

        self::assertNull($properties['_imagine']);
    }

    public function testNewObject()
    {
        $photo = new Photo([]);

        $prop = $this->getSecureProperty(Photo::class, '_imagine');

        self::assertNull($prop->getValue($photo));
    }

    public function testResetShouldCreateNewInstance()
    {
        $imagine = Photo::getImagine();

        self::assertTrue(is_object($imagine));

        self::assertSame($imagine, Photo::getImagine());

        $newImagine = Photo::resetImagineInstance();

        self::assertNotSame($imagine, $newImagine);
    }
}