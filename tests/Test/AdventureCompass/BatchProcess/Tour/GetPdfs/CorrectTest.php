<?php
namespace Test\AdventureCompass\BatchProcess\Tour\GetPdfs;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class CorrectTest extends UnitTestCase
{
    public function testCorrectPresetFolder()
    {
        $folderName = 'tests/datasets/folders/pdf/!success';

        $tour = new Tour($folderName);

        $pdfs = $tour->getPdfs();

        $expected = [
            $folderName . '/MapFile.pdf',
        ];

        self::assertSame($expected, $pdfs);
    }

    public function testCorrectSetFolder()
    {
        $folderName = 'tests/datasets/folders/pdf/!success';

        $tour = new Tour($folderName);

        $tour->setFolder($folderName . '2');

        $pdfs = $tour->getPdfs();

        $expected = [
            $folderName . '2/PresentationFile.pdf',
        ];

        self::assertSame($expected, $pdfs);
    }

    public function testCorrectCustomFolder()
    {
        $folderName = 'tests/datasets/folders/pdf/!success';

        $tour = new Tour($folderName);

        $pdfs = $tour->getPdfs($folderName . '2');

        $expected = [
            $folderName . '2/PresentationFile.pdf',
        ];

        self::assertSame($expected, $pdfs);
    }
}