<?php
namespace Test\AdventureCompass\BatchProcess\Tour\GetPdfs;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Tour;

class InvalidTest extends UnitTestCase
{
    public function testEmpty()
    {
        $folderName = 'tests/datasets/folders/pdf/!empty';

        $tour = new Tour($folderName);

        $expected = [];

        $pdfs = $tour->getPdfs();

        self::assertSame($expected, $pdfs);

        $pdfs = $tour->getPdfs($folderName);

        self::assertSame($expected, $pdfs);
    }

    public function testNoFileName()
    {
        $folderName = 'tests/datasets/folders/pdf/!noname';

        $tour = new Tour($folderName);

        $expected = [];

        $pdfs = $tour->getPdfs();

        self::assertSame($expected, $pdfs);

        $pdfs = $tour->getPdfs($folderName);

        self::assertSame($expected, $pdfs);
    }

    public function testNoFileExt()
    {
        $folderName = 'tests/datasets/folders/pdf/!noext';

        $tour = new Tour($folderName);

        $expected = [];

        $pdfs = $tour->getPdfs();

        self::assertSame($expected, $pdfs);

        $pdfs = $tour->getPdfs($folderName);

        self::assertSame($expected, $pdfs);
    }

    public function testInvalidExt()
    {
        $folderName = 'tests/datasets/folders/pdf/!invalidext';

        $tour = new Tour($folderName);

        $expected = [];

        $pdfs = $tour->getPdfs();

        self::assertSame($expected, $pdfs);

        $pdfs = $tour->getPdfs($folderName);

        self::assertSame($expected, $pdfs);
    }
}