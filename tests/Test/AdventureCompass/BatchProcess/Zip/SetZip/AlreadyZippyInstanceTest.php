<?php
namespace Test\AdventureCompass\BatchProcess\Zip\SetZip;

use Test\UnitTestCase;

use Alchemy\Zippy;

use AdventureCompass\BatchProcess\Zip;

class AlreadyZippyInstanceTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testThroughConstruct()
    {
        $zippy = Zippy\Zippy::load();

        $archive = $zippy->open('tests/datasets/zips/empty.zip');

        $zip = new Zip($archive);

        $prop = $this->getSecureProperty($zip, '_zip');

        self::assertTrue(is_object($prop->getValue($zip)));

        self::assertInstanceOf(Zippy\Archive\Archive::class, $prop->getValue($zip));

        self::assertSame($prop->getValue($zip), $archive);
        self::assertSame($zip->getZip(), $archive);
    }

    public function testReassign()
    {
        $oldZippy = Zippy\Zippy::load();

        $oldArchive = $oldZippy->open('tests/datasets/zips/empty.zip');

        $zip = new Zip($oldArchive);

        $prop = $this->getSecureProperty($zip, '_zip');

        self::assertTrue(is_object($zip->getZip()));

        self::assertInstanceOf(Zippy\Archive\Archive::class, $zip->getZip());

        self::assertSame($prop->getValue($zip), $oldArchive);
        self::assertSame($zip->getZip(), $oldArchive);

        $newZippy = Zippy\Zippy::load();

        $newArchive = $newZippy->open('tests/datasets/zips/empty.zip');

        $zip->setZip($newArchive);

        self::assertTrue(is_object($zip->getZip()));

        self::assertInstanceOf(Zippy\Archive\Archive::class, $zip->getZip());

        self::assertNotSame($oldArchive, $newArchive);

        self::assertSame($newArchive, $zip->getZip());
    }
}