<?php
namespace Test\AdventureCompass\BatchProcess\Zip\SetZip;

use Test\UnitTestCase;

use Alchemy\Zippy;

use AdventureCompass\BatchProcess\Zip;

class AsStringTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testThroughConstruct()
    {
        $zip = new Zip('tests/datasets/zips/empty.zip');

        $prop = $this->getSecureProperty($zip, '_zip');

        self::assertTrue(is_object($prop->getValue($zip)));

        self::assertInstanceOf(Zippy\Archive\Archive::class, $prop->getValue($zip));
    }

    public function testReassign()
    {
        $zip = new Zip('tests/datasets/zips/empty.zip');

        self::assertTrue(is_object($zip->getZip()));

        self::assertInstanceOf(Zippy\Archive\Archive::class, $zip->getZip());

        $oldZippy = $zip->getZip();

        $zip->setZip('tests/datasets/zips/empty.zip');

        self::assertNotSame($oldZippy, $zip->getZip());
    }
}