<?php
namespace Test\AdventureCompass\BatchProcess\Zip\Process;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class EmptyArchiveTest extends UnitTestCase
{
    public function testNoContent()
    {
        $zip = new Zip('tests/datasets/zips/empty.zip');

        $processed = $zip->process();

        self::assertCount(0, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals([], $processed);
    }

    public function testEmptySingleFolder()
    {
        $zip = new Zip('tests/datasets/zips/single_folder.zip');

        $processed = $zip->process();

        self::assertCount(0, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals([], $processed);
    }
}