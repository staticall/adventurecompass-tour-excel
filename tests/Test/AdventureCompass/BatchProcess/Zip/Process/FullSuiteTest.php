<?php
namespace Test\AdventureCompass\BatchProcess\Zip\Process;

use Symfony\Component\Filesystem\Filesystem;
use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class FullSuiteTest extends UnitTestCase
{
    protected $_tempPath;

    public function setUp()
    {
        parent::setUp();

        $this->_tempPath = 'tests/temp/' . uniqid() . '/';
    }

    public function tearDown()
    {
        $fs = new Filesystem;

        $fs->remove($this->_tempPath);

        parent::tearDown();
    }

    public function testOneTourFlat()
    {
        $zip = new Zip('tests/datasets/zips/tour/single_tour_flat.zip');

        $tempPath = rtrim($this->_tempPath, '/');

        $zip->setTempPath($tempPath);

        $processed = $zip->process();

        $expected = [
            'NameTour1' => self::getValidTourData($tempPath . '/NameTour1'),
        ];

        self::assertTrue(is_array($processed));

        self::assertCount(1, $processed);

        self::assertEquals($expected, $processed);
    }

    public function testMultiToursFlat()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_flat.zip');

        $tempPath = rtrim($this->_tempPath, '/');

        $zip->setTempPath($tempPath);

        $processed = $zip->process();

        $expected = [
            'NameTour1' => self::getValidTourData($tempPath . '/NameTour1'),
            'NameTour2' => self::getValidTourData($tempPath . '/NameTour2'),
        ];

        self::assertTrue(is_array($processed));

        self::assertCount(2, $processed);

        self::assertEquals($expected, $processed);
    }

    public function testOneTourWrapped()
    {
        $zip = new Zip('tests/datasets/zips/tour/single_tour_wrapped.zip');

        $tempPath = rtrim($this->_tempPath, '/');

        $zip->setTempPath($tempPath);

        $processed = $zip->process();

        $expected = [
            'Uploud tour/NameTour1' => self::getValidTourData($tempPath . '/Uploud tour/NameTour1'),
        ];

        self::assertTrue(is_array($processed));

        self::assertCount(1, $processed);

        self::assertEquals($expected, $processed);
    }

    public function testMultiToursWrapped()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_wrapped.zip');

        $tempPath = rtrim($this->_tempPath, '/');

        $zip->setTempPath($tempPath);

        $processed = $zip->process();

        $expected = [
            'Uploud tour/NameTour1' => self::getValidTourData($tempPath . '/Uploud tour/NameTour1'),
            'Uploud tour/NameTour2' => self::getValidTourData($tempPath . '/Uploud tour/NameTour2'),
        ];

        self::assertTrue(is_array($processed));

        self::assertCount(2, $processed);

        self::assertEquals($expected, $processed);
    }

    static public function getValidTourData($tempPath)
    {
        return [
            'tour'   => [
                'GeneralInfo'         => [
                    'TOUR NAME'          => 'Kunisaki and Yufuin Walk',
                    'ORIGINAL TOUR LINK' => 'http://www.360-expeditions.com/climbing/trips/europe/russia/Elbrus_Seven_summits_climb/itinerary',
                    'DESTINATION'        => 'Russia Federation',
                    'TYPE OF ADVENTURE'  => 'Walk',
                    'GUIDE LANGUAGE'     => 'Russia',
                    'ROUTE COMPLEXITY'   => 5,
                    'DURATION'           => 14,
                    'MAP FILE'           => 'MapFile.pdf',
                    'PRESENTATION FILE'  => 'PresentationFile.pdf',
                    'TOUR PRICE MIN'     => 0.00,
                    'TOUR PRICE MAX'     => 0.00,
                    'GROUP SIZE MIN'     => 0,
                    'GROUP SIZE MAX'     => 0,
                ],
                'Overview'            => [
                    'OVERVIEW DESCRIPTION' => 'Climb Elbrus the highest mountain in Europe. We reach the summit of Mount Elbrus via the pristine North-South traverse adhering to the climbing principles of tackling the mountain on its own terms. The satisfaction that comes from climbing with a small team of equally focused and dedicated mountaineers is extremely rewarding.

The summit is always a bonus but having gained it through your own “blood, sweat and tears” is much more satisfying and memorable than taking the chairlift to the top! Mount Elbrus is a huge double-domed dormant volcano located in the spectacular Caucasus Mountains of Russia. Starting on the Northern side of the mountain we begin a gradual ascent of the mountain, carrying our personal gear and a share of the teams’ camping equipment.

Taking our time to acclimatise we eventually reach our high camp where we refresh our glacier training, we trek to the summit col before heading up to the Western Summit where an amazing panorama awaits. Finally taking advantage of the aging ski infrastructure on the southern side we have a much faster descent to the distinctly Russian flavoured alpine village where we can relax after the climb. Elbru',
                ],
                'Itinerary'           => [
                    [
                        'NUM DAY'     => 1,
                        'TITLE DAY'   => 'Depart UK',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 2,
                        'TITLE DAY'   => 'Arrive in Russia',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 3,
                        'TITLE DAY'   => 'Mineralnye Vody - Emanuil Valley',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 4,
                        'TITLE DAY'   => 'Emanuil Valley',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 5,
                        'TITLE DAY'   => 'Emanuil Valley - Camp 1',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 6,
                        'TITLE DAY'   => 'Acclimatisation ascent to Lenz\'s rocks',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 7,
                        'TITLE DAY'   => 'Rest Day',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 8,
                        'TITLE DAY'   => 'Contingency day',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 9,
                        'TITLE DAY'   => 'Summit day',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],

                    [
                        'NUM DAY'     => 10,
                        'TITLE DAY'   => 'Depart Russia',
                        'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
                    ],
                ],
                'DatePrice'           => [
                    [
                        'DATE: DEPARTURE'      => '2017-01-18 03:00:00',
                        'DATE: RETURN'         => '2017-01-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-02-18 03:00:00',
                        'DATE: RETURN'         => '2017-02-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-03-18 03:00:00',
                        'DATE: RETURN'         => '2017-03-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-04-18 03:00:00',
                        'DATE: RETURN'         => '2017-04-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-05-18 03:00:00',
                        'DATE: RETURN'         => '2017-05-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-06-18 03:00:00',
                        'DATE: RETURN'         => '2017-06-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-07-18 03:00:00',
                        'DATE: RETURN'         => '2017-07-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-08-18 03:00:00',
                        'DATE: RETURN'         => '2017-08-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],

                    [
                        'DATE: DEPARTURE'      => '2017-09-18 03:00:00',
                        'DATE: RETURN'         => '2017-09-20 03:00:00',
                        'DURATION:'            => 2,
                        'PRICE (INCL. FLIGHT)' => 0.00,
                        'PRICE (EXCL. FLIGHT)' => 0.00,
                    ],
                ],
                'IncludedNotIncluded' => [
                    'INCLUDED'     =>
                        [
                            'International flights',
                            'Scheduled hotel nights, based on double or triple occupancy',
                            'Food while on the mountain',
                            'Scheduled group restaurant meals',
                            'Group climbing and cooking gear',
                            'Airport transfer to hotel',
                        ],
                    'NOT INCLUDED' =>
                        [
                            'Personal equipment and excess baggage for domestic flights',
                            'Staff/guide gratuities',
                            'Items of a personal nature: phone calls, laundry, room service, etc.',
                            'Alcoholic beverages',
                            'Additional single accommodation (post exped).',
                            'Lunch and dinner when city based except where indicated',
                        ],
                ],
                'KitList'             => [
                    [
                        'COUNT'       => 1,
                        'NAME'        => 'Backpack',
                        'DESCRIPTION' => 'Approximately 80L to take your kit to higher camps carrying up to 15kg',
                    ],

                    [
                        'COUNT'       => 2,
                        'NAME'        => 'Dry Sacks',
                        'DESCRIPTION' => 'Pack some fresh clothing into bags to keep them dry in the event of a total downpour that seeps into your kitbag. Good for quarantining old socks',
                    ],

                    [
                        'COUNT'       => 1,
                        'NAME'        => 'Kit Bag',
                        'DESCRIPTION' => 'A large duffel bag of 80 - 120L or more to transport your kit. Suitcases and wheeled bags are NOT suitable',
                    ],

                    [
                        'COUNT'       => 1,
                        'NAME'        => 'Sleepeng Bag Liner',
                        'DESCRIPTION' => 'These liners can be fleece or silk. They can increase the warmth of the sleeping bag and help to keep it clean',
                    ],

                    [
                        'COUNT'       => 1,
                        'NAME'        => 'Sleeping Matt',
                        'DESCRIPTION' => 'Full length rather than ¾ length ‘self-inflating’ Thermarest or Mammut',
                    ],

                    [
                        'COUNT'       => 1,
                        'NAME'        => 'Small Bag',
                        'DESCRIPTION' => 'This is for any kit you intend to leave at the hotel and could even simply be a heavy duty plastic bag',
                    ],
                ],
                'Accommodation'       => [
                    'ACCOMMODATION DESCRIPTION' => 'Climb Elbrus the highest mountain in Europe. We reach the summit of Mount Elbrus via the pristine North-South traverse adhering to the climbing principles of tackling the mountain on its own terms. The satisfaction that comes from climbing with a small team of equally focused and dedicated mountaineers is extremely rewarding.

The summit is always a bonus but having gained it through your own “blood, sweat and tears” is much more satisfying and memorable than taking the chairlift to the top! Mount Elbrus is a huge double-domed dormant volcano located in the spectacular Caucasus Mountains of Russia. Starting on the Northern side of the mountain we begin a gradual ascent of the mountain, carrying our personal gear and a share of the teams’ camping equipment.

Taking our time to acclimatise we eventually reach our high camp where we refresh our glacier training, we trek to the summit col before heading up to the Western Summit where an amazing panorama awaits. Finally taking advantage of the aging ski infrastructure on the southern side we have a much faster descent to the distinctly Russian flavoured alpine village where we can relax after the climb. Elbru',
                ],
                'Faq'                 => [
                    [
                        'QUESTION' => 'Can I bring my skis?',
                        'ANSWER'   => 'While it is possible to ski from the summit, we do not offer a ski trip on Elbrus except as a private trip. Contact our office for details.',
                    ],

                    [
                        'QUESTION' => 'How many climbers will be on this expedition?',
                        'ANSWER'   => 'Typically 9 to 12 members will be on the trip with you.',
                    ],
                ],
                'Testimonials'        => [
                    [
                        'AUTHOR NAME'        => 'Barry White',
                        'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
                    ],

                    [
                        'AUTHOR NAME'        => 'Barry White',
                        'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
                    ],

                    [
                        'AUTHOR NAME'        => 'Barry White',
                        'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
                    ],

                    [
                        'AUTHOR NAME'        => 'Barry White',
                        'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
                    ],

                    [
                        'AUTHOR NAME'        => 'Barry White',
                        'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
                    ],
                ],
                'Custom'              => [
                    [
                        'Enter the title of the section' => 'Enter the content of the section',
                    ],

                    [
                        'Enter the title of the section' => 'Enter the content of the section',
                    ],
                ],
            ],
            'photos' => [
                $tempPath . '/PHOTO/1.jpeg' => $tempPath . '/PHOTO/1.jpeg',
                $tempPath . '/PHOTO/2.jpeg' => $tempPath . '/PHOTO/2.jpeg',
                $tempPath . '/PHOTO/3.jpeg' => $tempPath . '/PHOTO/3.jpeg',
                $tempPath . '/PHOTO/4.jpeg' => $tempPath . '/PHOTO/4.jpeg',
            ],
            'videos' => [
            ],
            'pdfs'   => [
                $tempPath . '/MapFile.pdf'          => $tempPath . '/MapFile.pdf',
                $tempPath . '/PresentationFile.pdf' => $tempPath . '/PresentationFile.pdf',
            ],
        ];
    }
}