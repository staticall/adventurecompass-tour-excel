<?php
namespace Test\AdventureCompass\BatchProcess\Zip\Constructor;

use Test\UnitTestCase;

use Alchemy\Zippy;

use AdventureCompass\BatchProcess\Zip;

class CorrectTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testDefaults()
    {
        $reflection = new \ReflectionClass(Zip::class);

        $defaults = $reflection->getDefaultProperties();

        self::assertNull($defaults['_zip']);
    }

    public function testCorrect()
    {
        $prop = $this->getSecureProperty(Zip::class, '_zip');

        $zip = new Zip('tests/datasets/zips/empty.zip');

        self::assertTrue(is_object($prop->getValue($zip)));

        self::assertInstanceOf(Zippy\Archive\Archive::class, $prop->getValue($zip));
    }
}