<?php
namespace Test\AdventureCompass\BatchProcess\Zip\Constructor;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class InvalidTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Zip\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'not_exists_for_sure.zip'
     */
    public function testNotExists()
    {
        new Zip('not_exists_for_sure.zip');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Zip\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'not_readable_for_sure.zip'
     */
    public function testNotReadable()
    {
        self::requireFilesystemPermissionSupported();

        new Zip('tests/datasets/zips/not_readable.zip');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Zip\InvalidArgumentException
     * @expectedExceptionMessage Either file doesn't exists or can not be read 'tests/'
     */
    public function testNotFile()
    {
        new Zip('tests/');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Zip\InvalidArgumentException
     * @expectedExceptionMessage Invalid archive passed, original error - Unable to open archive
     */
    public function testNotArchive()
    {
        new Zip('tests/datasets/test.txt');
    }
}