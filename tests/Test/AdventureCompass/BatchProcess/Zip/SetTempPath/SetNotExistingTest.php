<?php
namespace Test\AdventureCompass\BatchProcess\Zip\SetTempPath;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

use Symfony\Component\Filesystem\Filesystem;

class SetNotExistingTest extends UnitTestCase
{
    protected $_testablePath = 'tests/temp/123321/';

    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testPassingNullShouldRegenerateTempPath()
    {
        $fs = new Filesystem;

        $zip = new Zip('tests/datasets/zips/empty.zip');

        self::assertNull($zip->getTempPath());

        $path = $this->_testablePath;

        self::assertFalse($fs->exists($path));

        $zip->setTempPath($path);

        self::assertTrue(is_string($zip->getTempPath()));

        self::assertFalse(strpos('\\', $zip->getTempPath()));

        self::assertSame($path, $zip->getTempPath());

        self::assertTrue($fs->exists($path));
    }

    public function tearDown()
    {
        $fs = new Filesystem;

        if ($fs->exists($this->_testablePath)) {
            $fs->remove($this->_testablePath);
        }

        parent::tearDown();
    }
}