<?php
namespace Test\AdventureCompass\BatchProcess\Zip\SetTempPath;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class DefaultTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testThroughConstruct()
    {
        $zip = new Zip('tests/datasets/zips/empty.zip');

        $prop = $this->getSecureProperty($zip, '_tempPath');

        self::assertNull($prop->getValue($zip));
    }

    public function testUntamperedShouldReturnNull()
    {
        $zip = new Zip('tests/datasets/zips/empty.zip');

        self::assertNull($zip->getTempPath());
    }
}