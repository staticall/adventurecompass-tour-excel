<?php
namespace Test\AdventureCompass\BatchProcess\Zip\SetTempPath;

use AdventureCompass\Common;
use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class ThroughGenerateTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testPassingNullShouldRegenerateTempPath()
    {
        $zip = new Zip('tests/datasets/zips/empty.zip');

        self::assertNull($zip->getTempPath());

        $zip->setTempPath();

        self::assertTrue(is_string($zip->getTempPath()));

        self::assertFalse(strpos('\\', $zip->getTempPath()));

        self::assertStringStartsWith(Common::transformWinToLinuxPath(sys_get_temp_dir()), $zip->getTempPath());

        self::assertNotSame(Common::transformWinToLinuxPath(sys_get_temp_dir()), $zip->getTempPath());
    }
}