<?php
namespace Test\AdventureCompass\BatchProcess\Zip\Process;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class MatchesSingleFileTest extends UnitTestCase
{
    public function testWildcardShouldMatchOnlyOneFile()
    {
        $zip = new Zip('tests/datasets/zips/multi_folders_same_file.zip');

        $processed = $zip->getFiles('folder/*.txt');

        self::assertCount(1, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('folder/hello.txt', $processed[0]->getLocation());
    }

    public function testWildcardPartNameShouldMatchOnlyOneFile()
    {
        $zip = new Zip('tests/datasets/zips/multi_folders_same_file.zip');

        $processed = $zip->getFiles('folder/h*lo.txt');

        self::assertCount(1, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('folder/hello.txt', $processed[0]->getLocation());
    }

    public function testExactShouldMatchOnlyOneFile()
    {
        $zip = new Zip('tests/datasets/zips/multi_folders_same_file.zip');

        $processed = $zip->getFiles('folder/hello.txt');

        self::assertCount(1, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('folder/hello.txt', $processed[0]->getLocation());
    }

    public function testWildcardShouldMatchOnlyOneFileMultifolder()
    {
        $zip = new Zip('tests/datasets/zips/multi_folders.zip');

        $processed = $zip->getFiles('**/sub2/location.txt');

        self::assertCount(1, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('folder/subfolder/sub2/location.txt', $processed[0]->getLocation());
    }

    public function testWildcardPartNameShouldMatchOnlyOneFileMultifolder()
    {
        $zip = new Zip('tests/datasets/zips/multi_folders.zip');

        $processed = $zip->getFiles('**/sub2/l*.txt');

        self::assertCount(1, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('folder/subfolder/sub2/location.txt', $processed[0]->getLocation());
    }

    public function testExactShouldMatchOnlyOneFileMultifolder()
    {
        $zip = new Zip('tests/datasets/zips/multi_folders.zip');

        $processed = $zip->getFiles('**/location.txt');

        self::assertCount(1, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('folder/subfolder/sub2/location.txt', $processed[0]->getLocation());
    }

    public function testXlsxShouldMatchTour()
    {
        $zip = new Zip('tests/datasets/zips/tour/single_tour.zip');

        $processed = $zip->getFiles('*.xlsx');

        self::assertCount(1, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('PasportTour.xlsx', $processed[0]->getLocation());
    }
}