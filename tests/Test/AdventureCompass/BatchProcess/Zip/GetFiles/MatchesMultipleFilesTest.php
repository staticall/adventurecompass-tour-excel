<?php
namespace Test\AdventureCompass\BatchProcess\Zip\Process;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class MatchesMultipleFilesTest extends UnitTestCase
{
    public function testWildcardShouldMatchAllTxtFiles()
    {
        $zip = new Zip('tests/datasets/zips/multi_folders.zip');

        $processed = $zip->getFiles('**/*.txt');

        self::assertCount(2, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('folder/subfolder/sub2/location.txt', $processed[0]->getLocation());
        self::assertEquals('folder/subfolder/test.txt', $processed[1]->getLocation());
    }

    public function testShouldMatchTourXlsxFlat()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_flat.zip');

        $processed = $zip->getFiles('**/*.xlsx');

        self::assertCount(2, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('NameTour1/PasportTour.xlsx', $processed[0]->getLocation());
        self::assertEquals('NameTour2/PasportTour.xlsx', $processed[1]->getLocation());
    }

    public function testShouldMatchTourXlsxWrapped()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_wrapped.zip');

        $processed = $zip->getFiles('**/*.xlsx');

        self::assertCount(2, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('Uploud tour/NameTour1/PasportTour.xlsx', $processed[0]->getLocation());
        self::assertEquals('Uploud tour/NameTour2/PasportTour.xlsx', $processed[1]->getLocation());
    }

    public function testShouldMatchTourXlsxFlatDefaultGlob()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_flat.zip');

        $processed = $zip->getFiles(Zip::DEFAULT_GLOB_PATTERN);

        self::assertCount(2, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('NameTour1/PasportTour.xlsx', $processed[0]->getLocation());
        self::assertEquals('NameTour2/PasportTour.xlsx', $processed[1]->getLocation());
    }

    public function testShouldMatchTourXlsxWrappedDefaultGlob()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_wrapped.zip');

        $processed = $zip->getFiles(Zip::DEFAULT_GLOB_PATTERN);

        self::assertCount(2, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals('Uploud tour/NameTour1/PasportTour.xlsx', $processed[0]->getLocation());
        self::assertEquals('Uploud tour/NameTour2/PasportTour.xlsx', $processed[1]->getLocation());
    }
}