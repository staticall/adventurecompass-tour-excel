<?php
namespace Test\AdventureCompass\BatchProcess\Zip\GetFolderContents;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess\Zip;

class PresetTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testShouldReturnEmpty()
    {
        $zip = new Zip('tests/datasets/zips/empty.zip');

        $contents = $zip->getFolderContents('/');

        self::assertCount(0, $contents);

        self::assertSame([], $contents);
    }

    public function testTourFlatPaths()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_flat.zip');

        $mappedNames = [];

        foreach ($zip->getFiles() as $file) {
            if ($file->isDir()) {
                continue;
            }

            $mappedNames[$file->getLocation()] = $file;
        }

        $expected = [];

        foreach ($mappedNames as $loc => $file) {
            if (mb_strpos($loc, 'NameTour1/') === 0) {
                $expected[] = $file;
            }
        }

        self::assertEquals($expected, $zip->getFolderContents('NameTour1/')); // Equals because order doesn't matter
    }

    public function testTourWrappedPaths()
    {
        $zip = new Zip('tests/datasets/zips/tour/multi_tour_wrapped.zip');

        $mappedNames = [];

        foreach ($zip->getFiles() as $file) {
            if ($file->isDir()) {
                continue;
            }

            $mappedNames[$file->getLocation()] = $file;
        }

        $expected = [];

        foreach ($mappedNames as $loc => $file) {
            if (mb_strpos($loc, 'Uploud tour/NameTour1/') === 0) {
                $expected[] = $file;
            }
        }

        self::assertEquals($expected, $zip->getFolderContents('Uploud tour/NameTour1/')); // Equals because order doesn't matter
    }
}