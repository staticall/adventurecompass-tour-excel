<?php
namespace Test\AdventureCompass\BatchProcess\Process;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess;

class InvalidArchiveTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    public function testNoContent()
    {
        $bp = new BatchProcess('tests/datasets/zips/empty.zip');

        $processed = $bp->process();

        self::assertCount(0, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals([], $processed);
    }

    public function testEmptySingleFolder()
    {
        $bp = new BatchProcess('tests/datasets/zips/single_folder.zip');

        $processed = $bp->process();

        self::assertCount(0, $processed);

        self::assertTrue(is_array($processed));

        self::assertEquals([], $processed);
    }
}