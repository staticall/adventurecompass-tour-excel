<?php
namespace Test\AdventureCompass\BatchProcess\Process;

use Test\UnitTestCase;

use AdventureCompass\BatchProcess;

class NotArchiveTest extends UnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        self::checkExtension('zip');
    }

    /**
     * @expectedException \AdventureCompass\BatchProcess\Exception
     * @expectedExceptionMessage Error catched while processing, original error - Invalid archive passed, original error - Unable to open archive
     */
    public function testNotArchive()
    {
        $bp = new BatchProcess('tests/datasets/test.txt');

        $bp->process();
    }
}