<?php
namespace Test\AdventureCompass\Common;

use AdventureCompass\Common;
use Test\UnitTestCase;

class UsageTest extends UnitTestCase
{
    public function testDataset()
    {
        $dataset = [
            [
                'input'    => 'C:\\Windows',
                'expected' => 'C:/Windows',
            ],

            [
                'input'    => 'e:\\System Path\\',
                'expected' => 'E:/System Path/',
            ],

            [
                'input'    => '/e/etc/passwd',
                'expected' => '/e/etc/passwd',
            ],

            [
                'input'    => 'd:\\Windows/dataset',
                'expected' => 'D:/Windows/dataset',
            ],
        ];

        foreach ($dataset as $data) {
            self::assertSame($data['expected'], Common::transformWinToLinuxPath($data['input']));
        }
    }
}