<?php
namespace Test\AdventureCompass\Common\Classify;

use AdventureCompass\Common;

use Test\UnitTestCase;

class FromXlsxTest extends UnitTestCase
{
    public function testSpecialChars()
    {
        $dataset = [
            'GENERAL INFO'            => 'GeneralInfo',
            'OVERVIEW'                => 'Overview',
            'DATE & PRICE'            => 'DatePrice',
            'INCLUDED | NOT INCLUDED' => 'IncludedNotIncluded',
            'CUSTOM 1'                => 'Custom',
            'CUSTOM_1'                => 'Custom',
            'CUSTOM-1'                => 'Custom',
        ];

        foreach ($dataset as $input => $expected) {
            $result = Common::classify($input);

            self::assertSame($expected, $result);
        }
    }
}