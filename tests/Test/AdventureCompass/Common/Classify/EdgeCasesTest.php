<?php
namespace Test\AdventureCompass\Common\Classify;

use AdventureCompass\Common;

use Test\UnitTestCase;

class EdgeCasesTest extends UnitTestCase
{
    public function testEdgeCases()
    {
        $dataset = [
            ' CUSTOM'    => 'Custom',
            'MY-CLASS'   => 'MyClass',
            'My_Class'   => 'MyClass',
            'field name' => 'FieldName',
        ];

        foreach ($dataset as $input => $expected) {
            $result = Common::classify($input);

            self::assertSame($expected, $result);
        }
    }
}