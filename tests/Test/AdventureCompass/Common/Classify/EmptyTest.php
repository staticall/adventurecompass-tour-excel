<?php
namespace Test\AdventureCompass\Common\Classify;

use AdventureCompass\Common;

use Test\UnitTestCase;

class EmptyTest extends UnitTestCase
{
    public function testSpecialChars()
    {
        $dataset = [
            '&' => '',
            ' ' => '',
            '_' => '',
            '-' => '',
        ];

        foreach ($dataset as $input => $expected) {
            $result = Common::classify($input);

            self::assertSame($expected, $result);
        }
    }

    public function testInvalidChars()
    {
        $dataset = [
            ''  => '',
            '0' => '',
        ];

        foreach ($dataset as $input => $expected) {
            $result = Common::classify($input);

            self::assertSame($expected, $result);
        }
    }
}