<?php
namespace Test;

abstract class UnitTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @var bool
     *
     * @private
     */
    private $_isLoaded = false;

    /**
     * @var array
     *
     * @protected
     *
     * @static
     */
    static protected $_isAvailable = array();

    /**
     * Setup the test
     *
     * @return null
     *
     * @protected
     */
    protected function setUp()
    {
        parent::setUp();

        $this->_isLoaded = true;
    }

    /**
     * Check if the test case is setup properly
     *
     * @return null
     *
     * @throws \PHPUnit_Framework_IncompleteTestError
     */
    public function __destruct()
    {
        if (!$this->_isLoaded) {
            throw new \PHPUnit_Framework_IncompleteTestError('Please run parent::setUp()');
        }
    }

    /**
     * Checks if a particular extension is loaded and if not, marks the test as skipped
     *
     * @param string $extension Extension to check
     *
     * @return bool
     *
     * @static
     */
    static public function checkExtension($extension)
    {
        if (!extension_loaded($extension)) {
            self::markTestSkipped("Warning: Extension '{$extension}' is not loaded");

            return false;
        }

        return true;
    }

    /**
     * Checks if GD is available or not
     * If GD is not available, test will be marked as skipped
     *
     * @return bool
     */
    public function requireGd()
    {
        if (!array_key_exists('gd', self::$_isAvailable)) {
            self::$_isAvailable['gd'] = extension_loaded('gd') && function_exists('gd_info');
        }

        if (!self::$_isAvailable['gd']) {
            self::markTestSkipped('Warning: GD is not available');

            return false;
        }

        return true;
    }

    /**
     * Checks if Imagick is available or not
     * If Imagick is not available, test will be marked as skipped
     *
     * @return bool
     */
    public function requireImagick()
    {
        if (!array_key_exists('imagick', self::$_isAvailable)) {
            self::$_isAvailable['imagick'] = extension_loaded('imagick') || class_exists('Imagick');
        }

        if (!self::$_isAvailable['imagick']) {
            self::markTestSkipped('Warning: Imagick is not available');

            return false;
        }

        return true;
    }

    /**
     * Returns accessible protected or private method for testing
     *
     * @param string $className  Target class name
     * @param string $methodName Target method name
     *
     * @return \ReflectionMethod
     *
     * @throws \ReflectionException If method not exists
     *
     * @static
     */
    static public function getSecureMethod($className, $methodName)
    {
        $reflection = new \ReflectionClass($className);

        $method = $reflection->getMethod($methodName);

        $method->setAccessible(true);

        return $method;
    }

    /**
     * Returns accessible protected or private property for testing
     *
     * @param string $className    Target class name
     * @param string $propertyName Target property name
     *
     * @return \ReflectionProperty
     *
     * @throws \ReflectionException If method not exists
     *
     * @static
     */
    static public function getSecureProperty($className, $propertyName)
    {
        $reflection = new \ReflectionClass($className);

        $property = $reflection->getProperty($propertyName);

        $property->setAccessible(true);

        return $property;
    }

    static public function requireFilesystemPermissionSupported()
    {
        if (PHP_OS === 'WINNT') {
            self::markTestSkipped('It\'s not possible to check permissions on Windows correctly');
        }
    }
}