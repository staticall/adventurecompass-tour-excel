<?php
set_include_path(realpath(__DIR__) . PATH_SEPARATOR . get_include_path());

chdir(dirname(__DIR__));

ini_set('memory_limit', '128M');
set_time_limit(0);

ini_set('display_errors', 1);
error_reporting(E_ALL);

mb_internal_encoding('UTF-8');
setlocale(LC_ALL, 'en_US');