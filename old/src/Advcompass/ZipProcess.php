<?php
namespace Advcompass;

class ZipProcess
{
    /**
     * @var \ZipArchive|null
     */
    protected $_zip;

    public function __construct($file)
    {
        if (!$this->loadZipArchive($file)) {
            throw new \Exception('File "' . $file . '" is not readable');
        }
    }

    public function __destruct()
    {
        if ($this->_zip) {
            $this->_zip->close();
        }
    }

    public function loadZipArchive($file)
    {
        if (!is_readable($file) || !is_file($file)) {
            return false;
        }

        $zip = new \ZipArchive;

        try {
            if ($zip->open($file) === true) {
                $this->_zip = $zip;
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}