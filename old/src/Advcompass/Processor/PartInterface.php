<?php
namespace Advcompass\Processor;

interface PartInterface
{
    public function getData();
}