<?php
namespace Advcompass\Processor\Part;

use Advcompass\Processor\PartAbstract;

class GeneralInfo extends PartAbstract
{
    public function getData()
    {
        $sheet = $this->getSheet();

        $data = [];

        foreach ($sheet as $dataset) {
            $data[$dataset['A']] = $dataset['B'];
        }

        $data = $this->postPrepare($data);

        return $data;
    }

    public function postPrepare(array $data)
    {
        $data = self::processImplodedMinMaxValues($data, 'TOUR PRICE MIN:MAX', 'TOUR PRICE MIN', 'TOUR PRICE MAX');
        $data = self::processImplodedMinMaxValues($data, 'GROUP SIZE MIN:MAX', 'GROUP SIZE MIN', 'GROUP SIZE MAX');

        $data['TOUR PRICE MIN'] = (float)$data['TOUR PRICE MIN'];
        $data['TOUR PRICE MAX'] = (float)$data['TOUR PRICE MAX'];
        $data['GROUP SIZE MIN'] = (int)$data['GROUP SIZE MIN'];
        $data['GROUP SIZE MAX'] = (int)$data['GROUP SIZE MAX'];

        if (array_key_exists('ROUTE COMPLEXITY', $data)) {
            $data['ROUTE COMPLEXITY'] = (int)$data['ROUTE COMPLEXITY'];
        }

        if (array_key_exists('DURATION', $data)) {
            $data['DURATION'] = (int)$data['DURATION'];
        }

        return $data;
    }

    static public function processImplodedMinMaxValues(array $data, $key, $minLabel, $maxLabel)
    {
        if (empty($data[$key])) {
            return $data;
        }

        $value = explode(':', $data[$key]);

        foreach ($value as &$v) {
            $v = trim($v);
        }

        unset($v);

        $valueMin = $value[0];
        $valueMax = empty($value[1]) ? $value[0] : $value[1];

        unset($data[$key]);

        $data[$minLabel] = $valueMin;
        $data[$maxLabel] = $valueMax;

        return $data;
    }
}