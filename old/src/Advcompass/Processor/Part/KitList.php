<?php
namespace Advcompass\Processor\Part;

use Advcompass\Processor\PartAbstract;

class KitList extends PartAbstract
{
    public function postPrepare(array $data)
    {
        $data['COUNT'] = (int)$data['COUNT'];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(array $part)
    {
        $isValid = parent::isValid($part);

        if (!$isValid) {
            return false;
        }

        return $part['COUNT'] > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getMap()
    {
        return [
            'A' => 'COUNT',
            'B' => 'NAME',
            'C' => 'DESCRIPTION',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequired()
    {
        return [
            'COUNT',
            'NAME',
        ];
    }
}