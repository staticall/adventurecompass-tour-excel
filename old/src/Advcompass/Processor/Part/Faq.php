<?php
namespace Advcompass\Processor\Part;

use Advcompass\Processor\PartAbstract;

class Faq extends PartAbstract
{
    /**
     * {@inheritdoc}
     */
    public function getMap()
    {
        return [
            'A' => 'QUESTION',
            'B' => 'ANSWER',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequired()
    {
        return [
            'QUESTION',
        ];
    }
}