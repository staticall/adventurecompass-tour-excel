<?php
namespace Advcompass\Processor\Part;

use Advcompass\Processor\PartAbstract;

class Testimonials extends PartAbstract
{
    /**
     * {@inheritdoc}
     */
    public function getMap()
    {
        return [
            'A' => 'AUTHOR NAME',
            'B' => 'REVIEW DESCRIPTION',

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequired()
    {
        return [
            'AUTHOR NAME',
            'REVIEW DESCRIPTION',
        ];
    }
}