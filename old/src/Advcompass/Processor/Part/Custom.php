<?php
namespace Advcompass\Processor\Part;

use Advcompass\Processor\PartAbstract;

class Custom extends PartAbstract
{
    public function getData()
    {
        return $this->getDataFlatten();
    }
}