<?php
namespace Advcompass\Processor\Part;

use Advcompass\Processor\PartAbstract;

class Accommodation extends PartAbstract
{
    public function getData()
    {
        return $this->getDataFlatten();
    }

    public function getMap()
    {
        return [
            'A' => 'ACCOMMODATION DESCRIPTION',
        ];
    }

    public function getRequired()
    {
        return [
            'ACCOMMODATION DESCRIPTION',
        ];
    }
}