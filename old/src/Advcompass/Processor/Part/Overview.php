<?php
namespace Advcompass\Processor\Part;

use Advcompass\Processor\PartAbstract;

class Overview extends PartAbstract
{
    public function getData()
    {
        return $this->getDataFlatten();
    }

    public function getMap()
    {
        return [
            'A' => 'OVERVIEW DESCRIPTION',
        ];
    }

    public function getRequired()
    {
        return [
            'OVERVIEW DESCRIPTION',
        ];
    }
}