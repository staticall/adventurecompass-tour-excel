<?php
namespace Advcompass;

use Advcompass\Processor\Part\Custom;

class Processor
{
    /**
     * Creates a new instance of desired part class
     *
     * @param string                    $name
     * @param array|\PHPExcel_Worksheet $sheet
     *
     * @return \Advcompass\Processor\Part\Custom|\Advcompass\Processor\PartAbstract
     */
    static public function factory($name, $sheet)
    {
        $procesorNamespace = __NAMESPACE__ . '\\Processor\\Part\\';

        $name = str_replace($procesorNamespace, '', $name);

        $expectedPart = $procesorNamespace . self::classify($name);
        $customPart   = Custom::class;

        if (!class_exists($expectedPart)) {
            return new $customPart($sheet);
        }

        return new $expectedPart($sheet);
    }

    /**
     * Convert a word in to the format for a Doctrine class name
     * For example, both 'my_class' and 'MY_CLASS' will be transformed into 'MyClass'
     *
     * @param string $word Word to classify
     *
     * @return string
     */
    static public function classify($word)
    {
        $word = preg_replace('/[^a-zA-Z _-]/', '', $word);

        return str_replace(' ', '', ucwords(strtr(strtolower($word), "_-", '  ')));
    }
}