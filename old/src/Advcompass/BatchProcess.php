<?php
namespace Advcompass;

class BatchProcess
{
    static public function loadFile($xlsx)
    {
        if (!is_readable($xlsx) || !is_file($xlsx)) {
            throw new \Exception("Either file doesn't exists or can not be read '{$xlsx}'");
        }

        $reader = \PHPExcel_IOFactory::createReader('Excel2007');

        return $reader->load($xlsx);
    }

    static public function processExcel(\PHPExcel $phpexcel)
    {
        $sheets = $phpexcel->getAllSheets();

        $data = [];

        foreach ($sheets as $sheet) {
            $part = Processor::factory($sheet->getTitle(), $sheet);

            $reflection = new \ReflectionClass($part);

            $shortClassName = $reflection->getShortName();

            if (!empty($data[$shortClassName])) {
                if (empty($data[$shortClassName][0])) {
                    $data[$shortClassName] = [$data[$shortClassName]];
                }

                $data[$shortClassName][] = $part->getData();
            } else {

                $data[$shortClassName] = $part->getData();
            }
        }

        return $data;
    }
}