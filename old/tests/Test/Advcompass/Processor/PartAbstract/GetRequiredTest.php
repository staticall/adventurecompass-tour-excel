<?php
namespace Test\Advcompass\Processor\PartAbstract;

use Mock\Advcompass\Processor\Part\MockFromAbstract;

use Test\UnitTestCase;

class GetRequiredTest extends UnitTestCase
{
    public function testShouldBeEmpty()
    {
        $part = new MockFromAbstract([]);

        self::assertSame([], $part->getRequired());
    }
}