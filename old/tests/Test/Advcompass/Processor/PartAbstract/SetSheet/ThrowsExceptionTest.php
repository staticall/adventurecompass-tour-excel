<?php
namespace Test\Advcompass\Processor\PartAbstract\SetSheet;

use Advcompass\Processor\Part\Custom as Part;

use Test\UnitTestCase;

class ThrowsExceptionTest extends UnitTestCase
{
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsStringDuringConstruct()
    {
        new Part('test');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsStringDuringCall()
    {
        $part = new Part([]);

        $part->setSheet('test');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsNullDuringConstruct()
    {
        new Part(null);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testAsNullDuringCall()
    {
        $part = new Part([]);

        $part->setSheet(null);
    }
}