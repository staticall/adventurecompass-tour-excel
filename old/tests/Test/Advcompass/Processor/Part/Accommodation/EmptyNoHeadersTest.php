<?php
namespace Test\Advcompass\Processor\Part\Accommodation;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Accommodation as Part;

class EmptyNoHeadersTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/accommodation-empty-no-headers.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [];

        self::assertSame($expected, $result);
    }
}