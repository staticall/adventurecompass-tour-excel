<?php
namespace Test\Advcompass\Processor\Part\Testimonials;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Testimonials as Part;

class EmptyNameTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/testimonials-empty-name.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'AUTHOR NAME'        => 'Barry White1',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White2',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White3',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White5',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

        ];

        self::assertSame($expected, $result);
    }
}