<?php
namespace Test\Advcompass\Processor\Part\Testimonials;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Testimonials as Part;

class NoHeadersTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/testimonials-no-headers.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

            [
                'AUTHOR NAME'        => 'Barry White',
                'REVIEW DESCRIPTION' => 'What a fantastic highlights video of my expedition to Mt Elbrus. I cannot express enough what a great souvenir this videos is, as well as a fantastic vehicle for storytelling. Thank you!',
            ],

        ];

        self::assertSame($expected, $result);
    }
}