<?php
namespace Test\Advcompass\Processor\Part\Itinerary;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Itinerary as Part;

class OnlyOneDayTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/itinerary-only-one-day.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'NUM DAY'     => 1,
                'TITLE DAY'   => 'Depart UK',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}