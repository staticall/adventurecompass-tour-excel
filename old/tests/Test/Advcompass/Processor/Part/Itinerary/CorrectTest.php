<?php
namespace Test\Advcompass\Processor\Part\Itinerary;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Itinerary as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/itinerary.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'NUM DAY'     => 1,
                'TITLE DAY'   => 'Depart UK',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 2,
                'TITLE DAY'   => 'Arrive in Russia',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 3,
                'TITLE DAY'   => 'Mineralnye Vody - Emanuil Valley',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 4,
                'TITLE DAY'   => 'Emanuil Valley',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 5,
                'TITLE DAY'   => 'Emanuil Valley - Camp 1',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 6,
                'TITLE DAY'   => 'Acclimatisation ascent to Lenz\'s rocks',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 7,
                'TITLE DAY'   => 'Rest Day',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 8,
                'TITLE DAY'   => 'Contingency day',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 9,
                'TITLE DAY'   => 'Summit day',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
            [
                'NUM DAY'     => 10,
                'TITLE DAY'   => 'Depart Russia',
                'DESCRIPTION' => 'Board four wheel drives for early morning start to the Base Camp of Mount Elbrus. Fantastic scenery of huge canyons and local villages. A feeling of remoteness is tangible.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}