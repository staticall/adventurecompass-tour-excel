<?php
namespace Test\Advcompass\Processor\Part\GeneralInfo\ProcessImplodedMinMaxValues;

use Test\UnitTestCase;

use Advcompass\Processor\Part\GeneralInfo as Part;

class InvalidTest extends UnitTestCase
{
    public function testIsNull()
    {
        $data = [
            'TEST VALUE' => null,
        ];

        self::assertSame($data, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }

    public function testArrayKeyNotExists()
    {
        $data = [];

        self::assertSame($data, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }

    public function testEmptyString()
    {
        $data = [
            'TEST VALUE' => '',
        ];

        self::assertSame($data, Part::processImplodedMinMaxValues($data, key($data), 'MIN', 'MAX'));
    }
}