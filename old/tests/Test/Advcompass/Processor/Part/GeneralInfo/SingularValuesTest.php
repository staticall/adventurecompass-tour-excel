<?php
namespace Test\Advcompass\Processor\Part\GeneralInfo;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\GeneralInfo as Part;

class SingularValuesTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/general-info-singular-values.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'TOUR NAME'          => 'Kunisaki and Yufuin Walk',
            'ORIGINAL TOUR LINK' => 'http://www.360-expeditions.com/climbing/trips/europe/russia/Elbrus_Seven_summits_climb/itinerary',
            'DESTINATION'        => 'Russia Federation',
            'TYPE OF ADVENTURE'  => 'Walk',
            'GUIDE LANGUAGE'     => 'Russia',
            'ROUTE COMPLEXITY'   => 5,
            'DURATION'           => 14,
            'MAP FILE'           => 'MapFile.pdf',
            'PRESENTATION FILE'  => 'PresentationFile.pdf',
            'TOUR PRICE MIN'     => 12.00,
            'TOUR PRICE MAX'     => 12.00,
            'GROUP SIZE MIN'     => 9,
            'GROUP SIZE MAX'     => 9,
        ];

        self::assertSame($expected, $result);
    }
}