<?php
namespace Test\Advcompass\Processor\Part\Faq;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Faq as Part;

class EmptyQuestionTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/faq-empty-question.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'QUESTION' => 'Can I bring my skis?',
                'ANSWER'   => 'While it is possible to ski from the summit, we do not offer a ski trip on Elbrus except as a private trip. Contact our office for details.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}