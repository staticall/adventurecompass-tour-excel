<?php
namespace Test\Advcompass\Processor\Part\Faq;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Faq as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/faq.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'QUESTION' => 'Can I bring my skis?',
                'ANSWER'   => 'While it is possible to ski from the summit, we do not offer a ski trip on Elbrus except as a private trip. Contact our office for details.',
            ],

            [
                'QUESTION' => 'How many climbers will be on this expedition?',
                'ANSWER'   => 'Typically 9 to 12 members will be on the trip with you.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}