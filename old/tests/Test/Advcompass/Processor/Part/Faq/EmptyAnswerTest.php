<?php
namespace Test\Advcompass\Processor\Part\Faq;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Faq as Part;

class EmptyAnswerTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/faq-empty-answer.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            [
                'QUESTION' => 'Can I bring my skis?',
                'ANSWER'   => null,
            ],

            [
                'QUESTION' => 'How many climbers will be on this expedition?',
                'ANSWER'   => 'Typically 9 to 12 members will be on the trip with you.',
            ],
        ];

        self::assertSame($expected, $result);
    }
}