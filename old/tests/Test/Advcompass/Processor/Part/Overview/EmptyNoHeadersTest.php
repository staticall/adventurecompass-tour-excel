<?php
namespace Test\Advcompass\Processor\Part\Overview;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Overview as Part;

class EmptyNoHeadersTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/overview-empty-no-headers.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [];

        self::assertSame($expected, $result);
    }
}