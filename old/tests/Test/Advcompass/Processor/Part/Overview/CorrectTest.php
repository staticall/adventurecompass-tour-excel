<?php
namespace Test\Advcompass\Processor\Part\Overview;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Overview as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/overview.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'OVERVIEW DESCRIPTION' => 'Climb Elbrus the highest mountain in Europe. We reach the summit of Mount Elbrus via the pristine North-South traverse adhering to the climbing principles of tackling the mountain on its own terms. The satisfaction that comes from climbing with a small team of equally focused and dedicated mountaineers is extremely rewarding.

The summit is always a bonus but having gained it through your own “blood, sweat and tears” is much more satisfying and memorable than taking the chairlift to the top! Mount Elbrus is a huge double-domed dormant volcano located in the spectacular Caucasus Mountains of Russia. Starting on the Northern side of the mountain we begin a gradual ascent of the mountain, carrying our personal gear and a share of the teams’ camping equipment.

Taking our time to acclimatise we eventually reach our high camp where we refresh our glacier training, we trek to the summit col before heading up to the Western Summit where an amazing panorama awaits. Finally taking advantage of the aging ski infrastructure on the southern side we have a much faster descent to the distinctly Russian flavoured alpine village where we can relax after the climb. Elbru',
        ];

        self::assertSame($expected, $result);
    }
}