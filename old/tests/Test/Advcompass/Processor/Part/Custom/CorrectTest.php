<?php
namespace Test\Advcompass\Processor\Part\Custom;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Custom as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/custom.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'Enter the title of the section' => 'Enter the content of the section',
        ];

        self::assertSame($expected, $result);
    }
}