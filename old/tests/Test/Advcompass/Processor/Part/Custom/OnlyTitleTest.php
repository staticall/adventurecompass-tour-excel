<?php
namespace Test\Advcompass\Processor\Part\Custom;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Custom as Part;

class OnlyTitleTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/custom-only-title.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'Enter the content of the section' => null,
        ];

        self::assertSame($expected, $result);
    }
}