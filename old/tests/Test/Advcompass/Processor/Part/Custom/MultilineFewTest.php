<?php
namespace Test\Advcompass\Processor\Part\Custom;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\Custom as Part;

class MultilineFewTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/custom-multiline-few.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'Enter the title of the section' => 'Enter the content of the section
Enter the content of the section line #2
Enter the content of the section line #3',
        ];

        self::assertSame($expected, $result);
    }
}