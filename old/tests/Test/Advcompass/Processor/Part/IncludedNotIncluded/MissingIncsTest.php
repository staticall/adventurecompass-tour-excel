<?php
namespace Test\Advcompass\Processor\Part\IncludedNotIncluded;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\IncludedNotIncluded as Part;

class MissingIncsTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/included-or-not-missing-incs.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'INCLUDED' => [
                'International flights',
                'Food while on the mountain',
                'Group climbing and cooking gear',
                'Airport transfer to hotel',
            ],

            'NOT INCLUDED' => [
                'Personal equipment and excess baggage for domestic flights',
                'Staff/guide gratuities',
                'Items of a personal nature: phone calls, laundry, room service, etc.',
                'Alcoholic beverages',
                'Additional single accommodation (post exped).',
                'Lunch and dinner when city based except where indicated',
            ],
        ];

        self::assertSame($expected, $result);
    }
}