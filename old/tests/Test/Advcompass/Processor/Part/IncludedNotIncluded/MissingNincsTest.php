<?php
namespace Test\Advcompass\Processor\Part\IncludedNotIncluded;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\IncludedNotIncluded as Part;

class MissingNincsTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/included-or-not-missing-nincs.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'INCLUDED' => [
                'International flights',
                'Scheduled hotel nights, based on double or triple occupancy',
                'Food while on the mountain',
                'Scheduled group restaurant meals',
                'Group climbing and cooking gear',
                'Airport transfer to hotel',
            ],

            'NOT INCLUDED' => [
                'Staff/guide gratuities',
                'Additional single accommodation (post exped).',
                'Lunch and dinner when city based except where indicated',
            ],
        ];

        self::assertSame($expected, $result);
    }
}