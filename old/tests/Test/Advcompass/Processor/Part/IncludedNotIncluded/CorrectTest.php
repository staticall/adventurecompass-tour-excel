<?php
namespace Test\Advcompass\Processor\Part\IncludedNotIncluded;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\IncludedNotIncluded as Part;

class CorrectTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/included-or-not.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'INCLUDED' => [
                'International flights',
                'Scheduled hotel nights, based on double or triple occupancy',
                'Food while on the mountain',
                'Scheduled group restaurant meals',
                'Group climbing and cooking gear',
                'Airport transfer to hotel',
            ],

            'NOT INCLUDED' => [
                'Personal equipment and excess baggage for domestic flights',
                'Staff/guide gratuities',
                'Items of a personal nature: phone calls, laundry, room service, etc.',
                'Alcoholic beverages',
                'Additional single accommodation (post exped).',
                'Lunch and dinner when city based except where indicated',
            ],
        ];

        self::assertSame($expected, $result);
    }
}