<?php
namespace Test\Advcompass\Processor\Part\IncludedNotIncluded;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

use Advcompass\Processor\Part\IncludedNotIncluded as Part;

class IncludedOnlyTest extends UnitTestCase
{
    public function testParsedThroughXlsx()
    {
        $phpexcel = BatchProcess::loadFile('tests/datasets/passport/included-only.xlsx');

        $part = new Part($phpexcel->getActiveSheet());

        $result = $part->getData();

        $expected = [
            'INCLUDED' => [
                'International flights',
                'Scheduled hotel nights, based on double or triple occupancy',
                'Food while on the mountain',
                'Scheduled group restaurant meals',
                'Airport transfer to hotel',
            ],
        ];

        self::assertSame($expected, $result);
    }
}