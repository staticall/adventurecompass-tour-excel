<?php
namespace Test\Advcompass\Processor;

use Test\UnitTestCase;

use Advcompass\Processor;

class FactoryTest extends UnitTestCase
{
    public function testShouldReturnCustom()
    {
        $dataset = [
            'Custom',
            'Custom1',
            'Custom-1',
            'Custom 1',
            'CUSTOM1',
            'CUSTOM-1',
            'CUSTOM 1',
            'custom1',
            'custom-1',
            'custom 1',
            'UnexistingType',
            'IncludedNotInclud',
            Processor\Part\Custom::class,
        ];

        $expected = Processor::class . '\\Part\\Custom';

        foreach ($dataset as $type) {
            $result = Processor::factory($type, []);

            self::assertTrue(is_object($result));

            self::assertInstanceOf($expected, $result, 'Was expecting ' . $expected . ' from "' . $type . '", got: ' . get_class($result));
        }
    }

    public function testShouldReturnExistingType()
    {
        $dataset = [
            'Accommodation'                           => Processor\Part\Accommodation::class,
            Processor\Part\Accommodation::class       => Processor\Part\Accommodation::class,
            'DatePrice'                               => Processor\Part\DatePrice::class,
            Processor\Part\DatePrice::class           => Processor\Part\DatePrice::class,
            'Faq'                                     => Processor\Part\Faq::class,
            Processor\Part\Faq::class                 => Processor\Part\Faq::class,
            'GeneralInfo'                             => Processor\Part\GeneralInfo::class,
            Processor\Part\GeneralInfo::class         => Processor\Part\GeneralInfo::class,
            'IncludedNotIncluded'                     => Processor\Part\IncludedNotIncluded::class,
            Processor\Part\IncludedNotIncluded::class => Processor\Part\IncludedNotIncluded::class,
            'Itinerary'                               => Processor\Part\Itinerary::class,
            Processor\Part\Itinerary::class           => Processor\Part\Itinerary::class,
            'KitList'                                 => Processor\Part\KitList::class,
            Processor\Part\KitList::class             => Processor\Part\KitList::class,
            'Overview'                                => Processor\Part\Overview::class,
            Processor\Part\Overview::class            => Processor\Part\Overview::class,
            'Testimonials'                            => Processor\Part\Testimonials::class,
            Processor\Part\Testimonials::class        => Processor\Part\Testimonials::class,
        ];

        foreach ($dataset as $input => $expected) {
            $result = Processor::factory($input, []);

            self::assertTrue(is_object($result));

            self::assertInstanceOf($expected, $result, 'Was expecting ' . $expected . ' from "' . $input . '", got: ' . get_class($result));
        }
    }

    public function testClassifyFromExampleXlsx()
    {
        $dataset = [
            'GENERAL INFO'            => 'GeneralInfo',
            'OVERVIEW'                => 'Overview',
            'DATE & PRICE'            => 'DatePrice',
            'INCLUDED | NOT INCLUDED' => 'IncludedNotIncluded',
            ' CUSTOM'                 => 'Custom',
            'CUSTOM 1'                => 'Custom',
            'CUSTOM_1'                => 'Custom',
            'CUSTOM-1'                => 'Custom',
            '&'                       => '',
            ' '                       => '',
            '_'                       => '',
            '-'                       => '',
            'MY-CLASS'                => 'MyClass',
            'My_Class'                => 'MyClass',
            'field name'              => 'FieldName',
        ];

        foreach ($dataset as $input => $expected) {
            $result = Processor::classify($input);

            self::assertSame($expected, $result);
        }
    }
}