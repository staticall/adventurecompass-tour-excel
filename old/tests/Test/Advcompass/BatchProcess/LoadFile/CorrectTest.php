<?php
namespace Test\Advcompass\BatchProcess\LoadFile;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

class CorrectTest extends UnitTestCase
{
    public function testCorrectFile()
    {
        $result = BatchProcess::loadFile('tests/datasets/passport/full.xlsx');

        self::assertTrue(is_object($result));

        self::assertInstanceOf(\PHPExcel::class, $result);
    }
}