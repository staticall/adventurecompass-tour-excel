<?php
namespace Test\Advcompass\BatchProcess\LoadFile;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

class InvalidTest extends UnitTestCase
{
    /**
     * @expectedException \Exception
     */
    public function testNotExists()
    {
        BatchProcess::loadFile('not_exists_for_sure.xlsx');
    }

    /**
     * @expectedException \Exception
     */
    public function testNotReadable()
    {
        BatchProcess::loadFile('not_exists_for_sure.xlsx');
    }

    /**
     * @expectedException \Exception
     */
    public function testNotFile()
    {
        BatchProcess::loadFile('tests/');
    }
}