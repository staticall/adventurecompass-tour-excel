<?php
namespace Test\Advcompass\BatchProcess\LoadFile;

use Test\UnitTestCase;

use Advcompass\BatchProcess;

class InvalidPhpErrorTest extends UnitTestCase
{
    /**
     * @expectedException \ErrorException
     * @expectedExceptionMessage ZipArchive::getFromName(): Invalid or uninitialized Zip object
     * @expectedExceptionCode 2
     */
    public function testNotXlsx()
    {
        set_error_handler(
            function ($errno, $errstr) {
                throw new \ErrorException($errstr, $errno);
            },

            E_WARNING
        );

        BatchProcess::loadFile('tests/Mock/Advcompass/Processor/Part/MockFromAbstract.php');
    }

    public function tearDown()
    {
        restore_error_handler();
    }
}