<?php
namespace Advcompass\ZipProcess\LoadZipArchive;

use Test\UnitTestCase;

use Advcompass\ZipProcess;

class NotArchiveTest extends UnitTestCase
{
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage File "tests/datasets/test.txt" is not readable
     */
    public function testTextFile()
    {
        new ZipProcess('tests/datasets/test.txt');
    }
}